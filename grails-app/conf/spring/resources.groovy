import org.apache.shiro.cas.CasWithErrorFilter

// Place your Spring DSL code here
beans = {
    casFilter(CasWithErrorFilter)
}
