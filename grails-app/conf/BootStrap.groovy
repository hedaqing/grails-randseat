import base.shiro.ShiroRole
import base.shiro.ShiroUser
import grails.util.Environment

class BootStrap {

    def init = { servletContext ->
        ShiroRole adminRole = ShiroRole.findByName('ADMIN')
        if (!adminRole) {
            adminRole = new ShiroRole(name: 'ADMIN')
            adminRole.save(failOnError: true, flush: true)
        }
        adminRole.addToPermissions("*:*")

        ShiroRole managerRole = ShiroRole.findByName('MANAGER')
        if (!managerRole) {
            managerRole = new ShiroRole(name: 'MANAGER')
            managerRole.save(failOnError: true, flush: true)
        }
        managerRole.addToPermissions("exam:*")
        managerRole.addToPermissions("paper:*")
        managerRole.addToPermissions("examRoom:*")
        managerRole.addToPermissions("report:*")
        managerRole.addToPermissions("student:*")
        managerRole.addToPermissions("room:*")
        managerRole.addToPermissions("ranges:*")
        managerRole.addToPermissions("seat:*")

        ShiroRole userRole = ShiroRole.findByName('USER')
        if (!userRole) {
            userRole = new ShiroRole(name: 'USER')
            userRole.save(failOnError: true, flush: true)
        }
        userRole.addToPermissions("exam:*")
        userRole.addToPermissions("examRoom:addItem")
        userRole.addToPermissions("paper:addPaper")
        userRole.addToPermissions("report:*")
        userRole.addToPermissions("student:show")

        if(Environment.current == Environment.DEVELOPMENT) {
            ShiroUser adminUser = ShiroUser.findByUsername('admin')
            if (!adminUser) {
                adminUser = new ShiroUser(username: 'admin', fullname: 'admin')
                adminUser.changePassword('admin')
                adminUser.save(failOnError: true, flush: true)
                adminUser.addToRoles(adminRole)
            }

            ShiroUser normalUser = ShiroUser.findByUsername('user')
            if (!normalUser) {
                normalUser = new ShiroUser(username: 'user', fullname: 'user')
                normalUser.changePassword('user')
                normalUser.save(failOnError: true, flush: true)
                normalUser.addToRoles(userRole)
            }
        }

    }
    def destroy = {
    }
}
