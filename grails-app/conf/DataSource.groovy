dataSource {
    pooled = true
    jmxExport = true
    driverClassName = "org.h2.Driver"
    username = "sa"
    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
//    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
    singleSession = true // configure OSIV singleSession mode
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update"
            driverClassName = "com.mysql.jdbc.Driver"
            url = "jdbc:mysql://localhost:3306/randseat?useUnicode=true&characterEncoding=UTF-8"
            username = "root"
            password = "root"
        }
    }

    production {
        dataSource {
            dbCreate = "create-drop"
            pooled = false
            driverClassName = "com.mysql.jdbc.Driver"
            jndiName = "jdbc/randseat" // For Jetty
        }
    }
}
