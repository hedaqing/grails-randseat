import org.apache.shiro.SecurityUtils

def isAdmin = { ->
    SecurityUtils.subject.isPermitted("*:*")
}

def isManager = { ->
    SecurityUtils.subject.isPermitted("seat:*")
}

navigation = {
    app {
        dashboard(controller:'dashboard', action:'index', data:[icon: 'icon-dashboard'], visible: true)
        exam(controller: 'exam', action: 'index')
        paper(controller: 'paper', action: 'index')
        manage(visible: isManager){
            student(controller: 'student', action: 'index',visible: false)
            room(controller: 'room', action: 'index')
            ranges(controller: 'ranges', action: 'index')
            seats(controller: 'seat', action: 'index')
            examRoom(controller: 'examRoom',action: 'index',visible: false)
        }
        system(visible: isAdmin) {
            shiroUser(controller: 'shiroUser', action: 'index') {
                list()
                create()
                show()
                edit()
            }
            shiroRole(controller: 'shiroRole', action: 'index') {
                list()
                create()
                show()
                edit()
            }
            shiroRoleAuthorizeKey(controller: 'shiroRoleAuthorizeKey', action: 'index') {
                list()
                create()
                show()
                edit()
            }
            shiroRoleAuthorizeRecord(controller: 'shiroRoleAuthorizeRecord', action: 'index') {
                list()
                create()
                show()
                edit()
            }
        }
    }
}
