package randseat

class RandseatTagLib {
    static namespace = "randseat"
//    static defaultEncodeAs = [taglib:'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def selectRoom = {attrs, body ->
        def options = []
        Room.list(sort: 'code').each {
            options.add([id: it.id, name: "${it.name+'('+it.seatCount+')'}"])
        }

        attrs['from'] = options
        attrs['optionKey'] = 'id'
        attrs['optionValue'] = 'name'
        attrs['noSelection'] = ['' : message(code: 'default.selectPlease.label', default: 'Please select...')]
        out << g.select(attrs)
    }
}
