package randseat

import org.springframework.dao.DataIntegrityViolationException

class RangesController {

    def grailsUiExtensions

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        [rangesInstanceList: Ranges.list(params), rangesInstanceTotal: Ranges.count(), params: params]
    }

    def create() {
        [rangesInstance: new Ranges(params)]
    }

    def save() {
        def rangesInstance = new Ranges(params)
        if (!rangesInstance.save(flush: true)) {
            render(view: "create", model: [rangesInstance: rangesInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'ranges.label', default: 'Ranges'), rangesInstance])
        redirect(action: "show", id: rangesInstance.id)
    }

    def show(Long id) {
        def rangesInstance = Ranges.get(id)
        if (!rangesInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'ranges.label', default: 'Ranges'), id])
            redirect(action: "list")
            return
        }

        [rangesInstance: rangesInstance]
    }

    def edit(Long id) {
        def rangesInstance = Ranges.get(id)
        if (!rangesInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'ranges.label', default: 'Ranges'), id])
            redirect(action: "list")
            return
        }

        [rangesInstance: rangesInstance]
    }

    def update(Long id, Long version) {
        def rangesInstance = Ranges.get(id)
        if (!rangesInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'ranges.label', default: 'Ranges'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (rangesInstance.version > version) {
                    rangesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'ranges.label', default: 'Ranges')] as Object[],
                            "Another user has updated this Ranges while you were editing")
                render(view: "edit", model: [rangesInstance: rangesInstance])
                return
            }
        }

        bindData(rangesInstance, params)

        if (!rangesInstance.save(flush: true)) {
            render(view: "edit", model: [rangesInstance: rangesInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'ranges.label', default: 'Ranges'), rangesInstance])
        redirect(action: "show", id: rangesInstance.id)
    }

    def delete(Long id) {
        def rangesInstance = Ranges.get(id)
        if (!rangesInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'ranges.label', default: 'Ranges'), id])
            redirect(action: "list")
            return
        }

        try {
            rangesInstance.delete(flush: true)
            grailsUiExtensions.displayFlashMessage(text: 'default.deleted.message', args: [message(code: 'ranges.label', default: 'Ranges'), rangesInstance])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'ranges.label', default: 'Ranges'), rangesInstance])
            redirect(action: "show", id: id)
        }
    }

}
