package randseat

import grails.transaction.Transactional
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.jdbc.core.JdbcTemplate

class ExamController {

    def grailsUiExtensions
    def exportService
    def dataSource

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        params['sort']="startTime"
        params['order']="desc"
        [examInstanceList: Exam.list(params), examInstanceTotal: Exam.count(), params: params]
    }

    def create() {
        def roomInstanceList = Room.list()
        StringBuffer roomOptionStr = new StringBuffer()
        roomInstanceList.each {r->
            roomOptionStr.append("<option value='${r.id}'>${r}</option>")
        }
        [examInstance: new Exam(params),roomOptionStr: roomOptionStr.toString()]
    }

    @Transactional
    def save() {
        def examInstance = new Exam(params)
        /*
        def roomList = params.getList('room')
        def numList = params.getList('amount')
        def size = roomList.size()
        if (size != numList.size()){
            def roomInstanceList = Room.list()
            StringBuffer roomOptionStr = new StringBuffer()
            roomInstanceList.each {r->
                roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
            }
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: '请重新填写教室和教室所用座位数！')
            render(view: "create", model: [examInstance: examInstance,roomOptionStr: roomOptionStr.toString(),params: params])
            return
        }
        // seat number to use must less than the room has
        def rooms=[]
        for (def i=0;i<size;i++){
            def numToUse = numList.get(i).toString().toInteger()
            def roomInstance = Room.findById(roomList.get(i).toString().toInteger())
            if (rooms.contains(roomInstance)){
                def roomInstanceList = Room.list()
                StringBuffer roomOptionStr = new StringBuffer()
                roomInstanceList.each {r->
                    roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
                }
                grailsUiExtensions.displayFlashMessage(type: 'warning', text: "教室："+roomInstance.name+'重复选用')
                render(view: "create", model: [examInstance: examInstance,roomOptionStr: roomOptionStr.toString(),params: params])
                return
            }
            rooms.add(roomInstance)
            if ( numToUse>roomInstance.seatCount ){
                def roomInstanceList = Room.list()
                StringBuffer roomOptionStr = new StringBuffer()
                roomInstanceList.each {r->
                    roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
                }
                grailsUiExtensions.displayFlashMessage(type: 'warning', text: roomInstance.name+'座位数不足'+numToUse)
                render(view: "create", model: [examInstance: examInstance,roomOptionStr: roomOptionStr.toString(),params: params])
                return
            }
        }
*/
        examInstance.save(flush: true,failOnError: true)
/*

        // save to ExamRoom
        for (def i=0;i<size;i++){
            def examRoomInstance = new ExamRoom()
            examRoomInstance.exam = examInstance
            examRoomInstance.room = Room.findById(roomList.get(i).toString().toInteger())
            examRoomInstance.number = numList.get(i).toString().toInteger()
            examRoomInstance.save(failOnError: true,flush: true)
        }
*/

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'exam.label', default: 'Exam'), examInstance])
        redirect(action: "show", id: examInstance.id)
    }

    def show(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        def examRoomInstanceList = ExamRoom.findAllByExam(examInstance)
        def roomInstanceList = Room.list()
        StringBuffer roomOptionStr = new StringBuffer()
        roomInstanceList.each {r->
            roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
        }

        [examInstance: examInstance,
         examRoomInstanceList:examRoomInstanceList,
         roomOptionStr: roomOptionStr.toString(),
         studentTotalCount: Student.countByExam(examInstance),
         studentSortedCount: Student.countByExamAndIsSorted(examInstance,true),
         studentUnsortedCount: Student.countByExamAndIsSorted(examInstance,false)]
    }

    def edit(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }
        /*
        def examRoomInstanceList = ExamRoom.findAllByExam(examInstance)

        def roomInstanceList = Room.list()
        StringBuffer roomOptionStr = new StringBuffer()
        roomInstanceList.each {r->
            roomOptionStr.append("<option value='${r.id}'>${r}</option>")
        }*/

//        [examInstance: examInstance,examRoomInstanceList:examRoomInstanceList,roomOptionStr: roomOptionStr.toString()]
        [examInstance: examInstance]
    }

    @Transactional
    def update(Long id, Long version) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (examInstance.version > version) {
                    examInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'exam.label', default: 'Exam')] as Object[],
                            "Another user has updated this Exam while you were editing")
                render(view: "edit", model: [examInstance: examInstance])
                return
            }
        }
/*

        def roomList = params.getList('room')
        def numList = params.getList('amount')
        def size = roomList.size()
        if (size != numList.size()){
            def roomInstanceList = Room.list()
            StringBuffer roomOptionStr = new StringBuffer()
            roomInstanceList.each {r->
                roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
            }

            def examRoomInstanceList = ExamRoom.findAllByExam(examInstance)

            grailsUiExtensions.displayFlashMessage(type: 'warning', text: '请重新填写教室和教室所用座位数！')
            render(view: "create", model: [examInstance: examInstance,examRoomInstanceList:examRoomInstanceList,roomOptionStr: roomOptionStr.toString(),params: params])
            return
        }
        // seat number to use must less than the room has
        def rooms=[]
        for (def i=0;i<size;i++){
            def numToUse = numList.get(i).toString().toInteger()
            def roomInstance = Room.findById(roomList.get(i).toString().toInteger())
            if (rooms.contains(roomInstance)){
                def roomInstanceList = Room.list()
                StringBuffer roomOptionStr = new StringBuffer()
                roomInstanceList.each {r->
                    roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
                }
                grailsUiExtensions.displayFlashMessage(type: 'warning', text: "教室："+roomInstance.name+'重复选用')
                render(view: "create", model: [examInstance: examInstance,roomOptionStr: roomOptionStr.toString(),params: params])
                return
            }
            rooms.add(roomInstance)
            if ( numToUse>roomInstance.seatCount ){
                def roomInstanceList = Room.list()
                StringBuffer roomOptionStr = new StringBuffer()
                roomInstanceList.each {r->
                    roomOptionStr.append("<option value='${r.id}'>${r.name}</option>")
                }
                grailsUiExtensions.displayFlashMessage(type: 'warning', text: roomInstance.name+'座位数不足'+numToUse)
                render(view: "create", model: [examInstance: examInstance,roomOptionStr: roomOptionStr.toString(),params: params])
                return
            }
        }
*/

        bindData(examInstance, params)
        examInstance.save(flush: true,failOnError: true)
/*

        ExamRoom.executeUpdate("delete ExamRoom where exam=:exam",[exam: examInstance])

        // save to ExamRoom
        for (def i=0;i<size;i++){
            def examRoomInstance = new ExamRoom()
            examRoomInstance.exam = examInstance
            examRoomInstance.room = Room.findById(roomList.get(i).toString().toInteger())
            examRoomInstance.number = numList.get(i).toString().toInteger()
            examRoomInstance.save(failOnError: true,flush: true)
        }
*/

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'exam.label', default: 'Exam'), examInstance])
        redirect(action: "show", id: examInstance.id)
    }

    def delete(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        try {
            ExamRoom.executeUpdate("delete ExamRoom where exam=:exam",[exam: examInstance])
            Student.executeUpdate("delete Student where exam=:exam",[exam: examInstance])
            examInstance.delete(flush: true)
            grailsUiExtensions.displayFlashMessage(text: 'default.deleted.message', args: [message(code: 'exam.label', default: 'Exam'), examInstance])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'exam.label', default: 'Exam'), examInstance])
            redirect(action: "show", id: id)
        }
    }

    def studentSeatMap(Long id){
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }
        def examRoomInstanceList = ExamRoom.findAllByExam(examInstance,[sort: 'room', order: 'asc'])
        def tables = []
        if (examRoomInstanceList){
            for (int roomIndex=0;roomIndex<examRoomInstanceList.size();roomIndex++){
                StringBuilder sb = new StringBuilder()
                sb.append("")
                def roomInstance = examRoomInstanceList[roomIndex].room
                // get seat map
                String[][] seatMap = new String[roomInstance.width+1][roomInstance.height+1]
                def rangesInstanceList = Ranges.findAllByRoom(roomInstance,[sort: "seq", order: "desc"])
                if (rangesInstanceList){
                    rangesInstanceList.each {ranges ->
                        def seatInstanceList = Seat.findAllByRange(ranges)
                        if (seatInstanceList){
                            seatInstanceList.each {seat ->
                                Student std = seat.getStudent(examInstance)
                                def usable = seat.usable?1:0
                                if (std){
                                    def bottonStr = "<br />" + std.firstname + "<br />"+std.paper.name
                                    def togStr = "${std.firstname}-${std.idnumber}-${std.department}"
                                    seatMap[seat.col][seat.row]=[seat.id,seat.code,usable,bottonStr,togStr].join(":")
                                } else {
                                    seatMap[seat.col][seat.row]=[seat.id,seat.code,usable,"未用","未用"].join(":")
                                }
                            }
                        }
                    }
                }

                // 根据区域数组生成html
                sb.append("<table>")
                for (int col = 0 ; col <= roomInstance.width ; col++) {
                    sb.append("<tr>")
                    for (int row = 1; row < roomInstance.height+1; row++) {
                        sb.append("<td align='center'>")
                        if (seatMap[col][row]){
                            def attr = seatMap[col][row].split(":") // [seat.id,seat.code,usable]
                            def seatId = attr[0].toInteger()
                            def seatCode = attr[1].toInteger()
                            def seatUsable = attr[2].toInteger()
                            def bottonStr = attr[3].toString()
                            def togStr = attr[4].toString()
                            if (seatUsable){
                                sb.append("<button style='height:80px;width:80px' align='bottom' class='btn btn-info btn-mini btn-block btn-app no-radius' data-original-title='${togStr}' data-rel='tooltip' data-placement='right' id='seatId_${seatId}' name='seatId_${seatId}'>")
                                sb.append("<span class=\"badge badge-yellow\">${seatCode}</span>")
                                sb.append("<span><small>${bottonStr}</small></span>")
                            } else {
                                sb.append("<button style='height:80px;width:80px' class='btn btn-gray btn-mini btn-block disabled btn-app no-radius' data-original-title='不可用' data-rel='tooltip' data-placement='right'>")
                                sb.append("<span class=\"badge badge-yellow\">${seatCode}</span>")
                                sb.append("<span><small>${bottonStr}</small></span>")
                            }

                            sb.append("</button>")
                        }
                        sb.append("</td>")
                    }
                    sb.append("</tr>")
                }
                sb.append("</table>")
                sb.append("<hr />")
                tables.push(sb.toString())
            }
        }
        [examRoomInstanceList:examRoomInstanceList,seatMaps: tables]
    }

    @Transactional
    def randSeat(Long id){
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        // 获取考生集合、教室集合、考卷集合
        def studentInstanceList = Student.findAllByExam(examInstance)
        def examRoomInstanceList = ExamRoom.findAllByExam(examInstance,[sort: "room", order: "asc"])
        def paperInstanceList = Paper.findAllByExamAndInUse(examInstance,true)

        // 三个集合均不能为空
        if (!studentInstanceList || !examRoomInstanceList || !paperInstanceList){
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: '未导入学生或者未指定教室或者未创建试卷')
            redirect(action: "show", id: examInstance.id)
            return
        }

        // 判断座位数和考生数
        def seatCount = 0
        examRoomInstanceList.each {r ->
            seatCount += r.number
        }
        def studentCount = studentInstanceList.size()
        if (seatCount < studentCount){
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: '座位分配不足，请重新分配')
            redirect(action: "show", id: examInstance.id)
            return
        }

        // 获取座位集合
        ArrayList<Seat> seatInstanceList = new ArrayList<Seat>()
        for (int i=0;i<examRoomInstanceList.size();i++){
            def rangesInstanceList = Ranges.findAllByRoom(examRoomInstanceList[i].room,[sort: "seq", order: "asc"])
            def seatInstanceListO = Seat.findAllByUsableAndRangeInList(true,rangesInstanceList,[max: examRoomInstanceList[i].number, sort: "code", order: "asc"])
            seatInstanceList.addAll(seatInstanceListO)
        }
        // 打乱学生顺序
        Collections.shuffle(studentInstanceList)

        // 考卷绑定到座位上
        def paperCount = paperInstanceList.size()

        for (int i=0;i<studentCount;i++){
            studentInstanceList[i].seat = seatInstanceList[i]
            int paperIndex = (seatInstanceList[i].col+seatInstanceList[i].row)%paperCount
            studentInstanceList[i].paper=paperInstanceList[paperIndex]
            studentInstanceList[i].room = seatInstanceList[i].range.room
            studentInstanceList[i].isSorted = true
        }
        Student.saveAll(studentInstanceList)

        examInstance.isSorted = true
        examInstance.save(failOnError: true,flush: true)
        redirect(action: "show", id: id)
    }

    def cleanStudent(Long id){
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        Student.executeUpdate("delete Student where exam=:exam",[exam: examInstance])

        examInstance.isSorted = false
        examInstance.save(failOnError: true,flush: true)
        redirect(action: "show", id: id)
    }

    def seatMapReport(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        def reportParams = [:]
        reportParams['_file'] = 'seatMapReport'
        reportParams['_format'] = "PDF"
        reportParams['examId'] = id

        String uuid = UUID.randomUUID().toString()
        session.setAttribute(uuid, reportParams)

        String name = URLEncoder.encode("座位表.pdf", 'UTF-8')
        redirect(uri: "/report/uuidShow/${uuid}/${name}")
    }

    def checkInReport(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        def reportParams = [:]
        reportParams['_file'] = 'checkInReport'
        reportParams['_format'] = "PDF"
        reportParams['id'] = id

        String uuid = UUID.randomUUID().toString()
        session.setAttribute(uuid, reportParams)

        String name = URLEncoder.encode("签到表.pdf", 'UTF-8')
        redirect(uri: "/report/uuidShow/${uuid}/${name}")
    }

    def exportMoodleUserList(Long id) {
        def examInstance = Exam.get(id)
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), id])
            redirect(action: "list")
            return
        }

        if(!params?.format || params.format != "csv"){
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: '指定的导出格式错误')
            redirect(action: "show", id: id)
            return
        }

        // Formatter closure
        def upperCase = { domain, value ->
            return value.toUpperCase()
        }

        Map formatters = [username: upperCase]
        Map parameters = [encoding: "UTF-8", "separator": ","]

        def sql = "select std.username,std.password,std.email,std.firstname,std.lastname,std.idnumber,std.department, paper.ext_name as course1 from (select username,password,email,firstname,lastname,idnumber,department,paper_id from student where exam_id=${id}) std left join paper on std.paper_id=paper.id"

        def jdbc = new JdbcTemplate(dataSource)
        def studentList = jdbc.queryForList(sql)
        exportService.export(params.format.toString(), response,URLEncoder.encode(examInstance.name, 'UTF-8'),params.extension.toString(), studentList, formatters, parameters)
    }
}
