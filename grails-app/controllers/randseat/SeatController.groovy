package randseat

import org.springframework.dao.DataIntegrityViolationException

class SeatController {

    def grailsUiExtensions

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        [seatInstanceList: Seat.list(params), seatInstanceTotal: Seat.count(), params: params]
    }

    def create() {
        [seatInstance: new Seat(params)]
    }

    def save() {
        def seatInstance = new Seat(params)
        if (!seatInstance.save(flush: true)) {
            render(view: "create", model: [seatInstance: seatInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'seat.label', default: 'Seat'), seatInstance])
        redirect(action: "show", id: seatInstance.id)
    }

    def show(Long id) {
        def seatInstance = Seat.get(id)
        if (!seatInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'seat.label', default: 'Seat'), id])
            redirect(action: "list")
            return
        }

        [seatInstance: seatInstance]
    }

    def edit(Long id) {
        def seatInstance = Seat.get(id)
        if (!seatInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'seat.label', default: 'Seat'), id])
            redirect(action: "list")
            return
        }

        [seatInstance: seatInstance]
    }

    def update(Long id, Long version) {
        def seatInstance = Seat.get(id)
        if (!seatInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'seat.label', default: 'Seat'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (seatInstance.version > version) {
                    seatInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'seat.label', default: 'Seat')] as Object[],
                            "Another user has updated this Seat while you were editing")
                render(view: "edit", model: [seatInstance: seatInstance])
                return
            }
        }

        bindData(seatInstance, params)

        if (!seatInstance.save(flush: true)) {
            render(view: "edit", model: [seatInstance: seatInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'seat.label', default: 'Seat'), seatInstance])
        redirect(action: "show", id: seatInstance.id)
    }

    def delete(Long id) {
        def seatInstance = Seat.get(id)
        if (!seatInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'seat.label', default: 'Seat'), id])
            redirect(action: "list")
            return
        }

        try {
            seatInstance.delete(flush: true)
            grailsUiExtensions.displayFlashMessage(text: 'default.deleted.message', args: [message(code: 'seat.label', default: 'Seat'), seatInstance])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'seat.label', default: 'Seat'), seatInstance])
            redirect(action: "show", id: id)
        }
    }
/*
    def updateCol() {
        def seatInstanceList = Seat.all
        seatInstanceList.each {seat ->
            def newcol = seat.range.room.width - seat.col + 1
            seat.col = newcol
        }

        Seat.saveAll(seatInstanceList)
    }
*/
}
