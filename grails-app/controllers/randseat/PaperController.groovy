package randseat

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class PaperController {

    def grailsUiExtensions

    static allowedMethods = [save: "POST", update: "POST", delete: "POST", addPaper: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        params['sort'] = 'id'
        params['order']= 'desc'
        [paperInstanceList: Paper.list(params), paperInstanceTotal: Paper.count(), params: params]
    }

    def create() {
        [paperInstance: new Paper(params)]
    }

    def save() {
        def paperInstance = new Paper(params)
        if (!paperInstance.save(flush: true)) {
            render(view: "create", model: [paperInstance: paperInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'paper.label', default: 'Paper'), paperInstance])
        redirect(action: "show", controller: "exam", id: paperInstance.id)
    }

    def show(Long id) {
        def paperInstance = Paper.get(id)
        if (!paperInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'paper.label', default: 'Paper'), id])
            redirect(action: "list")
            return
        }

        [paperInstance: paperInstance]
    }

    def edit(Long id) {
        def paperInstance = Paper.get(id)
        if (!paperInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'paper.label', default: 'Paper'), id])
            redirect(action: "list")
            return
        }

        [paperInstance: paperInstance]
    }

    def update(Long id, Long version) {
        def paperInstance = Paper.get(id)
        if (!paperInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'paper.label', default: 'Paper'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (paperInstance.version > version) {
                    paperInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'paper.label', default: 'Paper')] as Object[],
                            "Another user has updated this Paper while you were editing")
                render(view: "edit", model: [paperInstance: paperInstance])
                return
            }
        }

        bindData(paperInstance, params)

        if (!paperInstance.save(flush: true)) {
            render(view: "edit", model: [paperInstance: paperInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'paper.label', default: 'Paper'), paperInstance])
        redirect(action: "show", controller: "exam", id: paperInstance.exam.id)
    }

    def delete(Long id) {
        def paperInstance = Paper.get(id)
        if (!paperInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'paper.label', default: 'Paper'), id])
            redirect(action: "list")
            return
        }

        try {
            paperInstance.delete(flush: true)
            redirect(action: "show", controller: "exam", id: paperInstance.exam.id)
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'paper.label', default: 'Paper'), paperInstance])
            redirect(action: "show", id: id)
        }
    }

    /**
     * for ajax
     */
    def addPaper(){
        def res=[:]
        def data=[:]

        def examInstance = Exam.findById(params.getInt('exam'))
        def name = params['name']
        def extName = params['extName']
        if (!examInstance) {
            res['message']='找不到该考试'
            data['code']=0
            data['res']=res
            render data as JSON
            return false
        }
        if (!name || !extName){
            res['message']='考卷名称和外部名称不能为空'
            data['code']=0
            data['res']=res
            render data as JSON
            return false
        }
        def paperInstance = new Paper()
        paperInstance.exam = examInstance
        paperInstance.name = name
        paperInstance.extName = extName
        paperInstance.inUse = true
        paperInstance.save(failOnError: true,flush: true)

        res['message']='添加成功'
        data['code']=1
        data['res']=res
        render data as JSON
    }
}
