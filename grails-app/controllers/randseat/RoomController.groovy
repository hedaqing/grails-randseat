package randseat

import org.springframework.dao.DataIntegrityViolationException

class RoomController {

    def grailsUiExtensions

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        [roomInstanceList: Room.list(params), roomInstanceTotal: Room.count(), params: params]
    }

    def create() {
        [roomInstance: new Room(params)]
    }

    def save() {
        def roomInstance = new Room(params)
        if (!roomInstance.save(flush: true)) {
            render(view: "create", model: [roomInstance: roomInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'room.label', default: 'Room'), roomInstance])
        redirect(action: "show", id: roomInstance.id)
    }

    def show(Long id) {
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'room.label', default: 'Room'), id])
            redirect(action: "list")
            return
        }

        [roomInstance: roomInstance]
    }

    def edit(Long id) {
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'room.label', default: 'Room'), id])
            redirect(action: "list")
            return
        }

        [roomInstance: roomInstance]
    }

    def update(Long id, Long version) {
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'room.label', default: 'Room'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (roomInstance.version > version) {
                    roomInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'room.label', default: 'Room')] as Object[],
                            "Another user has updated this Room while you were editing")
                render(view: "edit", model: [roomInstance: roomInstance])
                return
            }
        }

        bindData(roomInstance, params)

        if (!roomInstance.save(flush: true)) {
            render(view: "edit", model: [roomInstance: roomInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'room.label', default: 'Room'), roomInstance])
        redirect(action: "show", id: roomInstance.id)
    }

    def delete(Long id) {
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'room.label', default: 'Room'), id])
            redirect(action: "list")
            return
        }

        try {
            roomInstance.delete(flush: true)
            grailsUiExtensions.displayFlashMessage(text: 'default.deleted.message', args: [message(code: 'room.label', default: 'Room'), roomInstance])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'room.label', default: 'Room'), roomInstance])
            redirect(action: "show", id: id)
        }
    }

    def showSeatMap(Long id){
        def roomInstance = Room.get(id)
        if (!roomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'room.label', default: 'Room'), id])
            redirect(action: "list")
            return
        }

        String[][] seatMap = new String[roomInstance.width+1][roomInstance.height+1]
        def rangesInstanceList = Ranges.findAllByRoom(roomInstance,[sort: "seq", order: "desc"])
        if (rangesInstanceList){
            rangesInstanceList.each {ranges ->
                def seatInstanceList = Seat.findAllByRange(ranges)
                if (seatInstanceList){
                    seatInstanceList.each {seat ->
                        def usable = seat.usable?1:0
                        seatMap[seat.col][seat.row]=[seat.id,seat.code,usable].join(":")
                    }
                }
            }
        }

        // 根据区域数组生成html
        StringBuilder sb = new StringBuilder()
        sb.append("")
        sb.append("<table>")
        for (int col = 0; col <=roomInstance.width ; col++) {
            sb.append("<tr>")
            for (int row = 1; row < roomInstance.height+1; row++) {
                sb.append("<td align='center'>")
                if (seatMap[col][row]){
                    def attr = seatMap[col][row].split(":") // [seat.id,seat.code,usable]
                    def seatId = attr[0].toInteger()
                    def seatCode = attr[1].toInteger()
                    def seatUsable = attr[2].toInteger()
                    if (seatUsable){
//                        sb.append("${g:link(action: 'show',controller: 'seat',class: 'btn btn-info btn-mini btn-block btn-app no-radius',style: 'height:80px;width:80px',id: seatId)}")
                        sb.append("<a href=\"/randseat/seat/show/${seatId}\" style='height:80px;width:80px' align='bottom' class='btn btn-info btn-mini btn-block btn-app no-radius' data-original-title='可用' data-rel='tooltip' data-placement='right' id='seatId_${seatId}' name='seatId_${seatId}'>")
                        sb.append("<span class=\"badge badge-yellow\">${seatCode}</span>")
                    } else {
                        sb.append("<a href=\"/randseat/seat/show/${seatId}\" style='height:80px;width:80px' class='btn btn-gray btn-mini btn-block disabled btn-app no-radius' data-original-title='不可用' data-rel='tooltip' data-placement='right'>")
                        sb.append("<span class=\"badge badge-yellow\">不可用</span>")
                    }
                    sb.append("</button>")
                }
                sb.append("</td>")
            }
            sb.append("</tr>")
        }
        sb.append("</table>")

        [roomInstance: roomInstance,seatMap: sb.toString()]
    }

}
