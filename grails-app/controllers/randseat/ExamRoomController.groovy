package randseat

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class ExamRoomController {

    def grailsUiExtensions

    static allowedMethods = [save: "POST", update: "POST", delete: "POST",addItem: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        [examRoomInstanceList: ExamRoom.list(params), examRoomInstanceTotal: ExamRoom.count(), params: params]
    }

    def create() {
        [examRoomInstance: new ExamRoom(params)]
    }

    def save() {
        def examRoomInstance = new ExamRoom(params)
        if (!examRoomInstance.save(flush: true)) {
            render(view: "create", model: [examRoomInstance: examRoomInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.created.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), examRoomInstance])
        redirect(action: "show", id: examRoomInstance.id)
    }

    def show(Long id) {
        def examRoomInstance = ExamRoom.get(id)
        if (!examRoomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), id])
            redirect(action: "list")
            return
        }

        [examRoomInstance: examRoomInstance]
    }

    def edit(Long id) {
        def examRoomInstance = ExamRoom.get(id)
        if (!examRoomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), id])
            redirect(action: "list")
            return
        }

        [examRoomInstance: examRoomInstance]
    }

    def update(Long id, Long version) {
        def examRoomInstance = ExamRoom.get(id)
        if (!examRoomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (examRoomInstance.version > version) {
                    examRoomInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'examRoom.label', default: 'ExamRoom')] as Object[],
                            "Another user has updated this ExamRoom while you were editing")
                render(view: "edit", model: [examRoomInstance: examRoomInstance])
                return
            }
        }

        bindData(examRoomInstance, params)

        if (!examRoomInstance.save(flush: true)) {
            render(view: "edit", model: [examRoomInstance: examRoomInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), examRoomInstance])
        redirect(action: "show", controller: "exam", id: examRoomInstance.exam.id)
    }

    def delete(Long id) {
        def examRoomInstance = ExamRoom.get(id)
        if (!examRoomInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), id])
            redirect(action: "list")
            return
        }

        try {
            examRoomInstance.delete(flush: true)
            redirect(action: "show", controller: "exam", id: examRoomInstance.exam.id)
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'examRoom.label', default: 'ExamRoom'), examRoomInstance])
            redirect(action: "show", id: id)
        }
    }

    def addItem(){
        def res=[:]
        def data=[:]

        def examInstance = Exam.findById(params.getInt('exam'))
        def roomInstance = Room.findById(params.getInt('room'))
        def number = params.getInt('number')
        if (!examInstance || !roomInstance || !number){
            res['message']='找不到考试或者考场或者座位数错误'
            data['code']=0
            data['res']=res
            render data as JSON
            return false
        }

        if (number > roomInstance.seatCount){
            res['message']='座位使用数超过了实际数量'
            data['code']=0
            data['res']=res
            render data as JSON
            return false
        }

        def examRoomInstance = ExamRoom.findByExamAndRoom(examInstance,roomInstance)
        if (examRoomInstance){
            if (examRoomInstance.number!=number){
                examRoomInstance.number = number
                examRoomInstance.save(failOnError: true,flush: true)
                res['message']='更新成功'
                data['code']=1
                data['res']=res
                render data as JSON
                return false
            }
            res['message']='该记录已存在'
            data['code']=1
            data['res']=res
            render data as JSON
            return false
        } else {
            def examRoomInstanceNew = new ExamRoom([exam:examInstance,room:roomInstance,number:number])
            examRoomInstanceNew.save(failOnError: true,flush: true)
            res['message']='添加成功'
            data['code']=1
            data['res']=res
            render data as JSON
            return false
        }
    }
}
