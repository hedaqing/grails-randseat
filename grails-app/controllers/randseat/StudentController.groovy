package randseat

import grails.transaction.Transactional
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.jdbc.core.JdbcTemplate

class StudentController {

    def grailsUiExtensions
    def dataSource

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params['max'] = Math.min(max ?: 10, 100)
        def r = Student.createCriteria().list(params){
            if (params['exam']){
                def examInstance = Exam.findById(params.getInt('exam'))
                if (examInstance)
                    eq('exam',examInstance)
            }
        }
        [studentInstanceList: r, studentInstanceTotal: r.totalCount, params: params]
    }

    def create() {
        def examInstance = Exam.findById(params.getInt('exam'))
        if (!examInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), params['exam']])
            redirect(action: "list",controller: 'exam')
            return
        }
        def studentInstance = new Student()
        studentInstance.exam = examInstance
        [studentInstance: studentInstance]
    }

    /**
     * TODO 需要判断重复导入的情况
     * @return
     */
    @Transactional
    def save() {
        def examInstance = Exam.findById(params.getInt('exam'))
        if (!examInstance){
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'exam.label', default: 'Exam'), params['exam']])
            redirect(action: "list",controller: 'exam')
            return
        }

        String[] students = Student.executeQuery("select distinct idnumber from Student s where s.exam=:exam",[exam: examInstance])

        String[] itemList = params['items'].toString().trim().split("\\n")
        List<String> wrongItems = new ArrayList<String>()
        List<String> multiItems = new ArrayList<String>()
        int successCount=0,failCount=0,multiCount=0
        itemList.each { r ->
            String[] it = r.trim().split("\\s")
            List<String> itemAttr = new ArrayList<String>() //学号、姓名、班级
            for (int i=0;i<it.size();i++){
                String str = it[i].trim()
                if (str) itemAttr.add(str)
            }

            if (itemAttr.size()!=3){
                wrongItems.add(r)
                failCount++
            } else {
                // 判断是否有重复考生
                if (students.contains(itemAttr[0])){
                    multiItems.add(r)
                    multiCount++
                } else {
                    def jdbc = new JdbcTemplate(dataSource)
                    def sql = "insert into randseat.student (version,username,idnumber,firstname,lastname,password,email,department,exam_id,is_sorted) values(0,'${itemAttr[0]}','${itemAttr[0]}','${itemAttr[1]}','${itemAttr[1]}','123456','${itemAttr[0]}@no.email','${itemAttr[2]}',${examInstance.id},false)"
                    jdbc.execute(sql)
                    successCount ++
                }
            }
        }

        def message = ""
        def hasError=false
        if (failCount==0 && multiCount==0){
            message="全部导入成功"
            flash.message = message
        } else if (successCount==0){
            message="全部导入失败<br />重复导入的有：${multiItems.toString()}<br />导入失败的有：${wrongItems.toString()}"
            flash.message = message
            hasError=true
        } else {
            message="成功的有${successCount}条,导入失败的有${failCount}条,重复导入的有${multiCount}。<br />重复导入的有：${multiItems.toString()}<br />失败的有${wrongItems.toString()}"
            flash.message = message
            hasError=true
        }

        redirect(action: "show",controller: "exam", id: examInstance.id,hasError:hasError)
    }

    def show(Long id) {
        def studentInstance = Student.get(id)
        if (!studentInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), id])
            redirect(action: "list")
            return
        }

        [studentInstance: studentInstance]
    }

    def edit(Long id) {
        def studentInstance = Student.get(id)
        if (!studentInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), id])
            redirect(action: "list")
            return
        }

        [studentInstance: studentInstance]
    }

    def update(Long id, Long version) {
        def studentInstance = Student.get(id)
        if (!studentInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (studentInstance.version > version) {
                    studentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'student.label', default: 'Student')] as Object[],
                            "Another user has updated this Student while you were editing")
                render(view: "edit", model: [studentInstance: studentInstance])
                return
            }
        }

        bindData(studentInstance, params)

        if (!studentInstance.save(flush: true)) {
            render(view: "edit", model: [studentInstance: studentInstance])
            return
        }

        grailsUiExtensions.displayFlashMessage(text: 'default.updated.message', args: [message(code: 'student.label', default: 'Student'), studentInstance])
        redirect(action: "show", id: studentInstance.id)
    }

    def delete(Long id) {
        def studentInstance = Student.get(id)
        if (!studentInstance) {
            grailsUiExtensions.displayFlashMessage(type: 'warning', text: 'default.not.found.message', args: [message(code: 'student.label', default: 'Student'), id])
            redirect(action: "list")
            return
        }

        try {
            studentInstance.delete(flush: true)
            grailsUiExtensions.displayFlashMessage(text: 'default.deleted.message', args: [message(code: 'student.label', default: 'Student'), studentInstance])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            grailsUiExtensions.displayFlashMessage(type: 'error', text: 'default.not.deleted.message', args: [message(code: 'student.label', default: 'Student'), studentInstance])
            redirect(action: "show", id: id)
        }
    }
}
