
<%@ page import="randseat.Student" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'student.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="username"
                                  title="${message(code: 'student.username.label', default: 'Username')}" params="${params}"/>
                
                <g:sortableColumn property="idnumber"
                                  title="${message(code: 'student.idnumber.label', default: 'Idnumber')}" params="${params}"/>
                
                <g:sortableColumn property="firstname"
                                  title="${message(code: 'student.firstname.label', default: 'Firstname')}" params="${params}"/>
                
                <g:sortableColumn property="lastname"
                                  title="${message(code: 'student.lastname.label', default: 'Lastname')}" params="${params}"/>
                
                <g:sortableColumn property="password"
                                  title="${message(code: 'student.password.label', default: 'Password')}" params="${params}"/>
                
                <g:sortableColumn property="email"
                                  title="${message(code: 'student.email.label', default: 'Email')}" params="${params}"/>
                
                <g:sortableColumn property="department"
                                  title="${message(code: 'student.department.label', default: 'Department')}" params="${params}"/>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${studentInstanceList}" var="studentInstance">
                <ui:tr>
                    
                    <td><f:display bean="${studentInstance}" property="id"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="username"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="idnumber"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="firstname"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="lastname"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="password"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="email"/></td>
                    
                    <td><f:display bean="${studentInstance}" property="department"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${studentInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${studentInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
