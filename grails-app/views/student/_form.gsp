<%@ page import="randseat.Student" %>



<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'username', 'error')} required">
    <label for="username">
        <g:message code="student.username.label" default="Username"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="username" required="" value="${studentInstance?.username}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'idnumber', 'error')} required">
    <label for="idnumber">
        <g:message code="student.idnumber.label" default="Idnumber"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="idnumber" required="" value="${studentInstance?.idnumber}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'firstname', 'error')} required">
    <label for="firstname">
        <g:message code="student.firstname.label" default="Firstname"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="firstname" required="" value="${studentInstance?.firstname}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'lastname', 'error')} required">
    <label for="lastname">
        <g:message code="student.lastname.label" default="Lastname"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="lastname" required="" value="${studentInstance?.lastname}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'password', 'error')} ">
    <label for="password">
        <g:message code="student.password.label" default="Password"/>
        
    </label>
    <g:textField name="password" value="${studentInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'email', 'error')} ">
    <label for="email">
        <g:message code="student.email.label" default="Email"/>
        
    </label>
    <g:textField name="email" value="${studentInstance?.email}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'department', 'error')} ">
    <label for="department">
        <g:message code="student.department.label" default="Department"/>
        
    </label>
    <g:textField name="department" value="${studentInstance?.department}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'exam', 'error')} ">
    <label for="exam">
        <g:message code="student.exam.label" default="Exam"/>
        
    </label>
    <g:select id="exam" name="exam.id" from="${randseat.Exam.list()}" optionKey="id" value="${studentInstance?.exam?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'room', 'error')} ">
    <label for="room">
        <g:message code="student.room.label" default="Room"/>
        
    </label>
    <g:select id="room" name="room.id" from="${randseat.Room.list()}" optionKey="id" value="${studentInstance?.room?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'seat', 'error')} ">
    <label for="seat">
        <g:message code="student.seat.label" default="Seat"/>
        
    </label>
    <g:select id="seat" name="seat.id" from="${randseat.Seat.list()}" optionKey="id" value="${studentInstance?.seat?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: studentInstance, field: 'paper', 'error')} ">
    <label for="paper">
        <g:message code="student.paper.label" default="Paper"/>
        
    </label>
    <g:select id="paper" name="paper.id" from="${randseat.Paper.list()}" optionKey="id" value="${studentInstance?.paper?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

