
<%@ page import="randseat.Student" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${studentInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${studentInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${studentInstance?.username}">
            <dt><g:message code="student.username.label" default="Username"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="username"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.idnumber}">
            <dt><g:message code="student.idnumber.label" default="Idnumber"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="idnumber"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.firstname}">
            <dt><g:message code="student.firstname.label" default="Firstname"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="firstname"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.lastname}">
            <dt><g:message code="student.lastname.label" default="Lastname"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="lastname"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.password}">
            <dt><g:message code="student.password.label" default="Password"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="password"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.email}">
            <dt><g:message code="student.email.label" default="Email"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="email"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.department}">
            <dt><g:message code="student.department.label" default="Department"/></dt>
            
            <dd><f:display bean="${studentInstance}" property="department"/></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.exam}">
            <dt><g:message code="student.exam.label" default="Exam"/></dt>
            
            <dd><g:link controller="exam" action="show"
                        id="${studentInstance?.exam?.id}"><f:display bean="${studentInstance}"
                                                                           property="exam"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.room}">
            <dt><g:message code="student.room.label" default="Room"/></dt>
            
            <dd><g:link controller="room" action="show"
                        id="${studentInstance?.room?.id}"><f:display bean="${studentInstance}"
                                                                           property="room"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.seat}">
            <dt><g:message code="student.seat.label" default="Seat"/></dt>
            
            <dd><g:link controller="seat" action="show"
                        id="${studentInstance?.seat?.id}"><f:display bean="${studentInstance}"
                                                                           property="seat"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${studentInstance?.paper}">
            <dt><g:message code="student.paper.label" default="Paper"/></dt>
            
            <dd><g:link controller="paper" action="show"
                        id="${studentInstance?.paper?.id}"><f:display bean="${studentInstance}"
                                                                           property="paper"/></g:link></dd>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
