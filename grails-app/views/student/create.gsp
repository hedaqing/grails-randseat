<%@ page import="randseat.Student" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'student.label', default: 'Student')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link class="btn">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <ui:form action="save" >
    <fieldset>
        %{--<f:all bean="studentInstance"/>--}%
        <input class="hidden" id="exam" name="exam" value="${studentInstance?.exam.id}"/>
        <div class="widget-box">
            <div class="widget-header">
                <h5>导入数据</h5>
            </div>

            <div class="widget-body">
                <div class="widget-main">

                    <p>将表格复制粘贴到文本框，分别是：学号、姓名、班级，以空格或者TAB键隔开，行与行之间用回车隔开。</p>
                    <p>可直接从Excel表格中选中相关区域进行复制粘贴。</p>
                    <hr>
                    <g:textArea name="items" class="span12" value="${params.get('items')}" style="margin: 0px; height: 400px;" required="required"/>
                </div>
            </div>
        </div>
    </fieldset>
    <ui:actions>
        <ui:button type="submit" mode="primary">
            <i class="icon-ok icon-white"></i>
            <g:message code="default.button.create.label" default="Create"/>
        </ui:button>
    </ui:actions>
</ui:form>
</theme:zone>
</body>
</html>
