
<%@ page import="randseat.Ranges" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'ranges.label', default: 'Ranges')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-ranges" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                                  default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-ranges" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            
            <g:sortableColumn property="name"
                              title="${message(code: 'ranges.name.label', default: 'Name')}"/>
            
            <th><g:message code="ranges.room.label" default="Room"/></th>
            
            <g:sortableColumn property="seq"
                              title="${message(code: 'ranges.seq.label', default: 'Seq')}"/>
            
            <g:sortableColumn property="row"
                              title="${message(code: 'ranges.row.label', default: 'Row')}"/>
            
            <g:sortableColumn property="col"
                              title="${message(code: 'ranges.col.label', default: 'Col')}"/>
            
            <g:sortableColumn property="width"
                              title="${message(code: 'ranges.width.label', default: 'Width')}"/>
            
        </tr>
        </thead>
        <tbody>
        <g:each in="${rangesInstanceList}" status="i" var="rangesInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                
                <td><g:link action="show" id="${rangesInstance.id}">${fieldValue(bean: rangesInstance, field: "name")}</g:link></td>
                
                <td>${fieldValue(bean: rangesInstance, field: "room")}</td>
                
                <td>${fieldValue(bean: rangesInstance, field: "seq")}</td>
                
                <td>${fieldValue(bean: rangesInstance, field: "row")}</td>
                
                <td>${fieldValue(bean: rangesInstance, field: "col")}</td>
                
                <td>${fieldValue(bean: rangesInstance, field: "width")}</td>
                
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${rangesInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
