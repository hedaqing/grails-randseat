
<%@ page import="randseat.Ranges" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'ranges.label', default: 'Ranges')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${rangesInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${rangesInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${rangesInstance?.name}">
            <dt><g:message code="ranges.name.label" default="Name"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="name"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.room}">
            <dt><g:message code="ranges.room.label" default="Room"/></dt>
            
            <dd><g:link controller="room" action="show"
                        id="${rangesInstance?.room?.id}"><f:display bean="${rangesInstance}"
                                                                           property="room"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.seq}">
            <dt><g:message code="ranges.seq.label" default="Seq"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="seq"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.row}">
            <dt><g:message code="ranges.row.label" default="Row"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="row"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.col}">
            <dt><g:message code="ranges.col.label" default="Col"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="col"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.width}">
            <dt><g:message code="ranges.width.label" default="Width"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="width"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.height}">
            <dt><g:message code="ranges.height.label" default="Height"/></dt>
            
            <dd><f:display bean="${rangesInstance}" property="height"/></dd>
            
        </g:if>
        
        <g:if test="${rangesInstance?.seats}">
            <dt><g:message code="ranges.seats.label" default="Seats"/></dt>
            
            <g:each in="${rangesInstance.seats}" var="s">
                <dd><g:link controller="seat" action="show"
                            id="${s.id}">${s?.encodeAsHTML()}</g:link></dd>
            </g:each>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
