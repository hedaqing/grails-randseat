
<%@ page import="randseat.Ranges" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'ranges.label', default: 'Ranges')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'ranges.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="name"
                                  title="${message(code: 'ranges.name.label', default: 'Name')}" params="${params}"/>
                
                <th><g:message code="ranges.room.label" default="Room"/></th>
                
                <g:sortableColumn property="seq"
                                  title="${message(code: 'ranges.seq.label', default: 'Seq')}" params="${params}"/>
                
                <g:sortableColumn property="row"
                                  title="${message(code: 'ranges.row.label', default: 'Row')}" params="${params}"/>
                
                <g:sortableColumn property="col"
                                  title="${message(code: 'ranges.col.label', default: 'Col')}" params="${params}"/>
                
                <g:sortableColumn property="width"
                                  title="${message(code: 'ranges.width.label', default: 'Width')}" params="${params}"/>
                
                <g:sortableColumn property="height"
                                  title="${message(code: 'ranges.height.label', default: 'Height')}" params="${params}"/>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${rangesInstanceList}" var="rangesInstance">
                <ui:tr>
                    
                    <td><f:display bean="${rangesInstance}" property="id"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="name"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="room"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="seq"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="row"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="col"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="width"/></td>
                    
                    <td><f:display bean="${rangesInstance}" property="height"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${rangesInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${rangesInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
