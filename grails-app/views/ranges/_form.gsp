<%@ page import="randseat.Ranges" %>



<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="ranges.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${rangesInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'room', 'error')} required">
    <label for="room">
        <g:message code="ranges.room.label" default="Room"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="room" name="room.id" from="${randseat.Room.list()}" optionKey="id" required="" value="${rangesInstance?.room?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'seq', 'error')} required">
    <label for="seq">
        <g:message code="ranges.seq.label" default="Seq"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="seq" type="number" value="${rangesInstance.seq}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'row', 'error')} required">
    <label for="row">
        <g:message code="ranges.row.label" default="Row"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="row" type="number" value="${rangesInstance.row}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'col', 'error')} required">
    <label for="col">
        <g:message code="ranges.col.label" default="Col"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="col" type="number" value="${rangesInstance.col}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'width', 'error')} required">
    <label for="width">
        <g:message code="ranges.width.label" default="Width"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="width" type="number" value="${rangesInstance.width}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: rangesInstance, field: 'height', 'error')} required">
    <label for="height">
        <g:message code="ranges.height.label" default="Height"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="height" type="number" value="${rangesInstance.height}" required=""/>

</div>

