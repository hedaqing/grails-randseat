<!doctype html>
<html>
<head>
    <theme:layout name="home" />
    <title><g:message code="default.home.label" default="Home" /></title>
    <r:require modules="application" />
</head>
<body>
<theme:zone name="body">
    <div clsss="row-fluid">
        <div class="span3">
            &nbsp;
        </div>
        <div class="span6 widget-container-span ui-sortable">
            <br/>
            <br/>
            <div class="widget-box">
                <div class="widget-header header-color-orange">
                    <h6>${p.organization()}${p.siteName()}</h6>
                </div>

                <div class="widget-body">
                    <div class="widget-body-inner" style="display: block;">
                        <div class="widget-main" style="min-height: 200px">
                            <ul class="unstyled spaced">
                                <li><i class="icon-star blue"></i>欢迎使用${p.siteName()}，系统使用演示。</li>
                                <li>
                                    <ul>
                                        <li>机房管理</li>
                                        <li>考试座位和考卷分配</li>
                                        <li><a href="docs/help.html" target="_blank">使用说明</a></li>
                                    </ul>
                                </li>
                            </ul>
                            <ui:displayMessage/>
                        </div>
                        <div class="widget-toolbox padding-8 clearfix">
                            %{--<ui:button kind="anchor" mode="primary" class="btn-mini pull-left" controller="auth" action="cas">
                                登录
                            </ui:button>--}%
                            <ui:button kind="anchor" mode="primary" class="btn-mini pull-left" controller="exam" action="index">
                                <i class="icon-dashboard"></i>
                                进入管理系统
                            </ui:button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</theme:zone>
</body>
</html>