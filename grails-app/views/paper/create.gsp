<%@ page import="randseat.Paper" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'paper.label', default: 'Paper')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link class="btn">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <ui:form action="save" >
    <fieldset>
        <f:all bean="paperInstance" except="students"/>
    </fieldset>
    <ui:actions>
        <ui:button type="submit" mode="primary">
            <i class="icon-ok icon-white"></i>
            <g:message code="default.button.create.label" default="Create"/>
        </ui:button>
    </ui:actions>
</ui:form>
</theme:zone>
</body>
</html>
