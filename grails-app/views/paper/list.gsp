
<%@ page import="randseat.Paper" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'paper.label', default: 'Paper')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'paper.id.label', default: 'Id')}" params="${params}"/>
                
                <th><g:message code="paper.exam.label" default="Exam"/></th>
                
                <g:sortableColumn property="name"
                                  title="${message(code: 'paper.name.label', default: 'Name')}" params="${params}"/>
                
                <g:sortableColumn property="extName"
                                  title="${message(code: 'paper.extName.label', default: 'Ext Name')}" params="${params}"/>
                
                <g:sortableColumn property="inUse"
                                  title="${message(code: 'paper.inUse.label', default: 'In Use')}" params="${params}"/>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${paperInstanceList}" var="paperInstance">
                <ui:tr>
                    
                    <td><f:display bean="${paperInstance}" property="id"/></td>
                    
                    <td><f:display bean="${paperInstance}" property="exam"/></td>
                    
                    <td><f:display bean="${paperInstance}" property="name"/></td>
                    
                    <td><f:display bean="${paperInstance}" property="extName"/></td>
                    
                    <td><g:formatBoolean boolean="${paperInstance.inUse}"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${paperInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${paperInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
