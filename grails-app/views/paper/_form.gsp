<%@ page import="randseat.Paper" %>



<div class="fieldcontain ${hasErrors(bean: paperInstance, field: 'exam', 'error')} required">
    <label for="exam">
        <g:message code="paper.exam.label" default="Exam"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="exam" name="exam.id" from="${randseat.Exam.list()}" optionKey="id" required="" value="${paperInstance?.exam?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: paperInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="paper.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${paperInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: paperInstance, field: 'extName', 'error')} required">
    <label for="extName">
        <g:message code="paper.extName.label" default="Ext Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="extName" required="" value="${paperInstance?.extName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: paperInstance, field: 'inUse', 'error')} ">
    <label for="inUse">
        <g:message code="paper.inUse.label" default="In Use"/>
        
    </label>
    <g:checkBox name="inUse" value="${paperInstance?.inUse}" />

</div>

