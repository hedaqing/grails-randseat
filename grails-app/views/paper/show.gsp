
<%@ page import="randseat.Paper" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'paper.label', default: 'Paper')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${paperInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${paperInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${paperInstance?.exam}">
            <dt><g:message code="paper.exam.label" default="Exam"/></dt>
            
            <dd><g:link controller="exam" action="show"
                        id="${paperInstance?.exam?.id}"><f:display bean="${paperInstance}"
                                                                           property="exam"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${paperInstance?.name}">
            <dt><g:message code="paper.name.label" default="Name"/></dt>
            
            <dd><f:display bean="${paperInstance}" property="name"/></dd>
            
        </g:if>
        
        <g:if test="${paperInstance?.extName}">
            <dt><g:message code="paper.extName.label" default="Ext Name"/></dt>
            
            <dd><f:display bean="${paperInstance}" property="extName"/></dd>
            
        </g:if>
        
        <g:if test="${paperInstance?.inUse}">
            <dt><g:message code="paper.inUse.label" default="In Use"/></dt>
            
            <dd><g:formatBoolean boolean="${paperInstance?.inUse}"/></dd>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
