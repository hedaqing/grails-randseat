
<%@ page import="randseat.Room" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'room.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="name"
                                  title="${message(code: 'room.name.label', default: 'Name')}" params="${params}"/>
                
                <g:sortableColumn property="place"
                                  title="${message(code: 'room.place.label', default: 'Place')}" params="${params}"/>
                
                <g:sortableColumn property="code"
                                  title="${message(code: 'room.code.label', default: 'Code')}" params="${params}"/>
                
                <g:sortableColumn property="seatCount"
                                  title="${message(code: 'room.seatCount.label', default: 'Seat Count')}" params="${params}"/>
                
                <g:sortableColumn property="width"
                                  title="${message(code: 'room.width.label', default: 'Width')}" params="${params}"/>
                
                <g:sortableColumn property="height"
                                  title="${message(code: 'room.height.label', default: 'Height')}" params="${params}"/>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${roomInstanceList}" var="roomInstance">
                <ui:tr>
                    
                    <td><f:display bean="${roomInstance}" property="id"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="name"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="place"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="code"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="seatCount"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="width"/></td>
                    
                    <td><f:display bean="${roomInstance}" property="height"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${roomInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="showSeatMap" id="${roomInstance.id}">
                            <i class="icon-group"></i>
                            座位图
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${roomInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
