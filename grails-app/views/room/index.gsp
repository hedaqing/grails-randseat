
<%@ page import="randseat.Room" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-room" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                                  default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-room" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            
            <g:sortableColumn property="name"
                              title="${message(code: 'room.name.label', default: 'Name')}"/>
            
            <g:sortableColumn property="place"
                              title="${message(code: 'room.place.label', default: 'Place')}"/>
            
            <g:sortableColumn property="code"
                              title="${message(code: 'room.code.label', default: 'Code')}"/>
            
            <g:sortableColumn property="seatCount"
                              title="${message(code: 'room.seatCount.label', default: 'Seat Count')}"/>
            
            <g:sortableColumn property="width"
                              title="${message(code: 'room.width.label', default: 'Width')}"/>
            
            <g:sortableColumn property="height"
                              title="${message(code: 'room.height.label', default: 'Height')}"/>
            
        </tr>
        </thead>
        <tbody>
        <g:each in="${roomInstanceList}" status="i" var="roomInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                
                <td><g:link action="show" id="${roomInstance.id}">${fieldValue(bean: roomInstance, field: "name")}</g:link></td>
                
                <td>${fieldValue(bean: roomInstance, field: "place")}</td>
                
                <td>${fieldValue(bean: roomInstance, field: "code")}</td>
                
                <td>${fieldValue(bean: roomInstance, field: "seatCount")}</td>
                
                <td>${fieldValue(bean: roomInstance, field: "width")}</td>
                
                <td>${fieldValue(bean: roomInstance, field: "height")}</td>
                
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${roomInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
