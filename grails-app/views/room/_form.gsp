<%@ page import="randseat.Room" %>



<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="room.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${roomInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'place', 'error')} required">
    <label for="place">
        <g:message code="room.place.label" default="Place"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="place" required="" value="${roomInstance?.place}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'code', 'error')} required">
    <label for="code">
        <g:message code="room.code.label" default="Code"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="code" required="" value="${roomInstance?.code}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'seatCount', 'error')} required">
    <label for="seatCount">
        <g:message code="room.seatCount.label" default="Seat Count"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="seatCount" type="number" value="${roomInstance.seatCount}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'width', 'error')} required">
    <label for="width">
        <g:message code="room.width.label" default="Width"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="width" type="number" value="${roomInstance.width}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'height', 'error')} required">
    <label for="height">
        <g:message code="room.height.label" default="Height"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="height" type="number" value="${roomInstance.height}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'ranges', 'error')} ">
    <label for="ranges">
        <g:message code="room.ranges.label" default="Ranges"/>
        
    </label>
    
<ul class="one-to-many">
<g:each in="${roomInstance?.ranges?}" var="r">
    <li><g:link controller="ranges" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="ranges" action="create" params="['room.id': roomInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'ranges.label', default: 'Ranges')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: roomInstance, field: 'exams', 'error')} ">
    <label for="exams">
        <g:message code="room.exams.label" default="Exams"/>
        
    </label>
    
<ul class="one-to-many">
<g:each in="${roomInstance?.exams?}" var="e">
    <li><g:link controller="examRoom" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="examRoom" action="create" params="['room.id': roomInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'examRoom.label', default: 'ExamRoom')])}</g:link>
</li>
</ul>


</div>

