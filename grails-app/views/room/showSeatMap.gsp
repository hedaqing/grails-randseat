<%@ page import="randseat.Room" %>
<%@ page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}"/>
    <title>${roomInstance.name} 座位图</title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    ${seatMap}
</theme:zone>
</body>
</html>
