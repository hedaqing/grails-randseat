
<%@ page import="randseat.Room" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${roomInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:button kind="anchor" mode="primary" action="showSeatMap" id="${roomInstance?.id}">
            座位图
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${roomInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${roomInstance?.name}">
            <dt><g:message code="room.name.label" default="Name"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="name"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.place}">
            <dt><g:message code="room.place.label" default="Place"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="place"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.code}">
            <dt><g:message code="room.code.label" default="Code"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="code"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.seatCount}">
            <dt><g:message code="room.seatCount.label" default="Seat Count"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="seatCount"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.width}">
            <dt><g:message code="room.width.label" default="Width"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="width"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.height}">
            <dt><g:message code="room.height.label" default="Height"/></dt>
            
            <dd><f:display bean="${roomInstance}" property="height"/></dd>
            
        </g:if>
        
        <g:if test="${roomInstance?.ranges}">
            <dt><g:message code="room.ranges.label" default="Ranges"/></dt>
            
            <g:each in="${roomInstance.ranges}" var="r">
                <dd><g:link controller="ranges" action="show"
                            id="${r.id}">${r?.encodeAsHTML()}</g:link></dd>
            </g:each>
            
        </g:if>
        
        <g:if test="${roomInstance?.exams}">
            <dt><g:message code="room.exams.label" default="Exams"/></dt>
            
            <g:each in="${roomInstance.exams}" var="e">
                <dd><g:link controller="examRoom" action="show"
                            id="${e.id}">${e?.encodeAsHTML()}</g:link></dd>
            </g:each>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
