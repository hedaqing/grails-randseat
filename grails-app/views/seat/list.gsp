
<%@ page import="randseat.Seat" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'seat.label', default: 'Seat')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'seat.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="code"
                                  title="${message(code: 'seat.code.label', default: 'Code')}" params="${params}"/>
                
                <g:sortableColumn property="row"
                                  title="${message(code: 'seat.row.label', default: 'Row')}" params="${params}"/>
                
                <g:sortableColumn property="col"
                                  title="${message(code: 'seat.col.label', default: 'Col')}" params="${params}"/>
                
                <g:sortableColumn property="usable"
                                  title="${message(code: 'seat.usable.label', default: 'Usable')}" params="${params}"/>
                
                <th><g:message code="seat.range.label" default="Range"/></th>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${seatInstanceList}" var="seatInstance">
                <ui:tr>
                    
                    <td><f:display bean="${seatInstance}" property="id"/></td>
                    
                    <td><f:display bean="${seatInstance}" property="code"/></td>
                    
                    <td><f:display bean="${seatInstance}" property="row"/></td>
                    
                    <td><f:display bean="${seatInstance}" property="col"/></td>
                    
                    <td><g:formatBoolean boolean="${seatInstance.usable}"/></td>
                    
                    <td><f:display bean="${seatInstance}" property="range"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${seatInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${seatInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
