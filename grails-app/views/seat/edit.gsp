<%@ page import="randseat.Seat" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'seat.label', default: 'Seat')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <ui:form action="update" id="${seatInstance?.id}" >
    <g:hiddenField name="version" value="${seatInstance?.version}"/>
    <fieldset>
        <f:all bean="seatInstance" except="students"/>
    </fieldset>
    <ui:actions>
        <ui:button type="submit" mode="primary">
            <i class="icon-ok icon-white"></i>
            <g:message code="default.button.update.label" default="Update"/>
        </ui:button>
        <ui:button type="submit" mode="danger" name="_action_delete" formnovalidate="true" id="delete-button">
            <i class="icon-trash icon-white"></i>
            <g:message code="default.button.delete.label" default="Delete"/>
        </ui:button>
    </ui:actions>
</ui:form>
</theme:zone>
</body>
</html>
