
<%@ page import="randseat.Seat" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'seat.label', default: 'Seat')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${seatInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${seatInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${seatInstance?.code}">
            <dt><g:message code="seat.code.label" default="Code"/></dt>
            
            <dd><f:display bean="${seatInstance}" property="code"/></dd>
            
        </g:if>
        
        <g:if test="${seatInstance?.row}">
            <dt><g:message code="seat.row.label" default="Row"/></dt>
            
            <dd><f:display bean="${seatInstance}" property="row"/></dd>
            
        </g:if>
        
        <g:if test="${seatInstance?.col}">
            <dt><g:message code="seat.col.label" default="Col"/></dt>
            
            <dd><f:display bean="${seatInstance}" property="col"/></dd>
            
        </g:if>
        
        <g:if test="${seatInstance?.usable}">
            <dt><g:message code="seat.usable.label" default="Usable"/></dt>
            
            <dd><g:formatBoolean boolean="${seatInstance?.usable}"/></dd>
            
        </g:if>
        
        <g:if test="${seatInstance?.range}">
            <dt><g:message code="seat.range.label" default="Range"/></dt>
            
            <dd><g:link controller="ranges" action="show"
                        id="${seatInstance?.range?.id}"><f:display bean="${seatInstance}"
                                                                           property="range"/></g:link></dd>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
