<%@ page import="randseat.Seat" %>



<div class="fieldcontain ${hasErrors(bean: seatInstance, field: 'code', 'error')} required">
    <label for="code">
        <g:message code="seat.code.label" default="Code"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="code" type="number" value="${seatInstance.code}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: seatInstance, field: 'row', 'error')} required">
    <label for="row">
        <g:message code="seat.row.label" default="Row"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="row" type="number" value="${seatInstance.row}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: seatInstance, field: 'col', 'error')} required">
    <label for="col">
        <g:message code="seat.col.label" default="Col"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="col" type="number" value="${seatInstance.col}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: seatInstance, field: 'usable', 'error')} ">
    <label for="usable">
        <g:message code="seat.usable.label" default="Usable"/>
        
    </label>
    <g:checkBox name="usable" value="${seatInstance?.usable}" />

</div>

<div class="fieldcontain ${hasErrors(bean: seatInstance, field: 'range', 'error')} required">
    <label for="range">
        <g:message code="seat.range.label" default="Range"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="range" name="range.id" from="${randseat.Ranges.list()}" optionKey="id" required="" value="${seatInstance?.range?.id}" class="many-to-one"/>

</div>

