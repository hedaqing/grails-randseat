
<%@ page import="randseat.Seat" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'seat.label', default: 'Seat')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-seat" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                                  default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-seat" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            
            <g:sortableColumn property="code"
                              title="${message(code: 'seat.code.label', default: 'Code')}"/>
            
            <g:sortableColumn property="row"
                              title="${message(code: 'seat.row.label', default: 'Row')}"/>
            
            <g:sortableColumn property="col"
                              title="${message(code: 'seat.col.label', default: 'Col')}"/>
            
            <g:sortableColumn property="usable"
                              title="${message(code: 'seat.usable.label', default: 'Usable')}"/>
            
            <th><g:message code="seat.range.label" default="Range"/></th>
            
        </tr>
        </thead>
        <tbody>
        <g:each in="${seatInstanceList}" status="i" var="seatInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                
                <td><g:link action="show" id="${seatInstance.id}">${fieldValue(bean: seatInstance, field: "code")}</g:link></td>
                
                <td>${fieldValue(bean: seatInstance, field: "row")}</td>
                
                <td>${fieldValue(bean: seatInstance, field: "col")}</td>
                
                <td><g:formatBoolean boolean="${seatInstance.usable}"/></td>
                
                <td>${fieldValue(bean: seatInstance, field: "range")}</td>
                
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${seatInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
