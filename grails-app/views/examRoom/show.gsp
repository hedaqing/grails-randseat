
<%@ page import="randseat.ExamRoom" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'examRoom.label', default: 'ExamRoom')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="${examRoomInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${examRoomInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        
        <g:if test="${examRoomInstance?.number}">
            <dt><g:message code="examRoom.number.label" default="Number"/></dt>
            
            <dd><f:display bean="${examRoomInstance}" property="number"/></dd>
            
        </g:if>
        
        <g:if test="${examRoomInstance?.exam}">
            <dt><g:message code="examRoom.exam.label" default="Exam"/></dt>
            
            <dd><g:link controller="exam" action="show"
                        id="${examRoomInstance?.exam?.id}"><f:display bean="${examRoomInstance}"
                                                                           property="exam"/></g:link></dd>
            
        </g:if>
        
        <g:if test="${examRoomInstance?.room}">
            <dt><g:message code="examRoom.room.label" default="Room"/></dt>
            
            <dd><g:link controller="room" action="show"
                        id="${examRoomInstance?.room?.id}"><f:display bean="${examRoomInstance}"
                                                                           property="room"/></g:link></dd>
            
        </g:if>
        
    </dl>

</theme:zone>
</body>
</html>
