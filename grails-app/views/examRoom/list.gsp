
<%@ page import="randseat.ExamRoom" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'examRoom.label', default: 'ExamRoom')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'examRoom.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="number"
                                  title="${message(code: 'examRoom.number.label', default: 'Number')}" params="${params}"/>
                
                <th><g:message code="examRoom.exam.label" default="Exam"/></th>
                
                <th><g:message code="examRoom.room.label" default="Room"/></th>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${examRoomInstanceList}" var="examRoomInstance">
                <ui:tr>
                    
                    <td><f:display bean="${examRoomInstance}" property="id"/></td>
                    
                    <td><f:display bean="${examRoomInstance}" property="number"/></td>
                    
                    <td><f:display bean="${examRoomInstance}" property="exam"/></td>
                    
                    <td><f:display bean="${examRoomInstance}" property="room"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${examRoomInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${examRoomInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
