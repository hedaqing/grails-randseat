<%@ page import="randseat.ExamRoom" %>



<div class="fieldcontain ${hasErrors(bean: examRoomInstance, field: 'number', 'error')} required">
    <label for="number">
        <g:message code="examRoom.number.label" default="Number"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field name="number" type="number" value="${examRoomInstance.number}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: examRoomInstance, field: 'exam', 'error')} required">
    <label for="exam">
        <g:message code="examRoom.exam.label" default="Exam"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="exam" name="exam.id" from="${randseat.Exam.list()}" optionKey="id" required="" value="${examRoomInstance?.exam?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examRoomInstance, field: 'room', 'error')} required">
    <label for="room">
        <g:message code="examRoom.room.label" default="Room"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="room" name="room.id" from="${randseat.Room.list()}" optionKey="id" required="" value="${examRoomInstance?.room?.id}" class="many-to-one"/>

</div>

