
<%@ page import="randseat.ExamRoom" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'examRoom.label', default: 'ExamRoom')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-examRoom" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                                  default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-examRoom" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>
            
            <g:sortableColumn property="number"
                              title="${message(code: 'examRoom.number.label', default: 'Number')}"/>
            
            <th><g:message code="examRoom.exam.label" default="Exam"/></th>
            
            <th><g:message code="examRoom.room.label" default="Room"/></th>
            
        </tr>
        </thead>
        <tbody>
        <g:each in="${examRoomInstanceList}" status="i" var="examRoomInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                
                <td><g:link action="show" id="${examRoomInstance.id}">${fieldValue(bean: examRoomInstance, field: "number")}</g:link></td>
                
                <td>${fieldValue(bean: examRoomInstance, field: "exam")}</td>
                
                <td>${fieldValue(bean: examRoomInstance, field: "room")}</td>
                
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${examRoomInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
