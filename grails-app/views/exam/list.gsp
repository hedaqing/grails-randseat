
<%@ page import="randseat.Exam" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'exam.label', default: 'Exam')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                
                <g:sortableColumn property="id"
                                  title="${message(code: 'exam.id.label', default: 'Id')}" params="${params}"/>
                
                <g:sortableColumn property="name"
                                  title="${message(code: 'exam.name.label', default: 'Name')}" params="${params}"/>
                
                <g:sortableColumn property="startTime"
                                  title="${message(code: 'exam.startTime.label', default: 'Start Time')}" params="${params}"/>
                
                <g:sortableColumn property="endTime"
                                  title="${message(code: 'exam.endTime.label', default: 'End Time')}" params="${params}"/>
                
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${examInstanceList}" var="examInstance">
                <ui:tr>
                    
                    <td><f:display bean="${examInstance}" property="id"/></td>
                    
                    <td><f:display bean="${examInstance}" property="name"/></td>
                    
                    <td><g:formatDate date="${examInstance.startTime}" format="yyyy-MM-dd HH:mm"/></td>
                    
                    <td><g:formatDate date="${examInstance.endTime}" format="yyyy-MM-dd HH:mm"/></td>
                    
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="${examInstance.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="${examInstanceTotal}" params="${params}"/>
    </div>
</theme:zone>
</body>
</html>
