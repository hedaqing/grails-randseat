
<%@ page import="randseat.Student; randseat.Exam" %>
<%@page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'exam.label', default: 'Exam')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
    <r:require module="export"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="create" controller="student" params="${[exam:examInstance.id]}">
            <i class="icon-pencil"></i>
            添加考生
        </ui:button>
        <ui:button kind="anchor" mode="primary" action="edit" id="${examInstance?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="${examInstance?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <g:if test="${flash.message}" >
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">
                <i class="icon-remove"></i>
            </button>
            ${flash.message.toString().replaceAll("\n", "<br/>")}
        </div>
    </g:if>
    <h4 class="light-blue">基本信息</h4>
    <ui:table class="table-striped table-bordered table-hover">
        <ui:tr>
            <th><g:message code="exam.name.label" default="Name"/></th>
            <th><g:message code="exam.startTime.label" default="Start Time"/></th>
            <th><g:message code="exam.endTime.label" default="End Time"/></th>
        </ui:tr>
        <ui:tr>
            <td><f:display bean="${examInstance}" property="name"/></td>
            <td><g:formatDate date="${examInstance?.startTime}" format="yyyy-MM-dd HH:mm"/></td>
            <td><g:formatDate date="${examInstance?.endTime}" format="yyyy-MM-dd HH:mm"/></td>
        </ui:tr>
    </ui:table>

    <div class="row-fluid">
        <div class="span12">
            <h4 class="light-green">
                考场情况&nbsp;&nbsp;&nbsp;&nbsp;
                <small>
                    <a href="#addRoom-form" id="addRoom" name="addRoom"  data-toggle="modal" >添加考场</a>&nbsp;&nbsp;&nbsp;&nbsp;
                </small>
                <g:if test="${examInstance.isSorted}">
                    <small>
                        <g:link action="seatMapReport" controller="exam" id="${examInstance.id}">
                            下载座位表
                        </g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                    </small>
                    <small>
                        <g:link action="checkInReport" controller="exam" id="${examInstance.id}">
                            下载签到表
                        </g:link>
                    </small>
                </g:if>
            </h4>
        </div>
    </div>
    <ui:table class="table-striped table-bordered table-hover">
        <ui:tr>
            <th><g:message code="exam.rooms.label" default="Rooms"/></th>
            <th><g:message code="examRoom.number.label" default="Use Seat"/></th>
            <th><g:message code="room.seatCount.label" default="Total Seat count"/></th>
            <th>Options</th>
        </ui:tr>
        <g:each in="${examInstance.rooms}" var="room">
            <ui:tr>
                <td><f:display bean="${room.room}" property="name"/></td>
                <td><f:display bean="${room}" property="number"/></td>
                <td><f:display bean="${room.room}" property="seatCount"/></td>
                <td>

                    <ui:button kind="anchor" mode="info" class="btn-mini" action="edit" controller="examRoom" id="${room.id}">
                        <i class="icon-info-sign"></i>
                        <g:message code="default.button.edit.label" default="Edit" />
                    </ui:button>

                    <ui:form action="delete" class="inline" controller="examRoom">
                        <g:hiddenField name="id" value="${room.id}"/>
                        <ui:button mode="danger" class="btn-mini" type="submit" name="_action_delete" id="delete-button">
                            <i class="icon-trash icon-white"></i>
                            <g:message code="default.button.delete.label" default="Delete"/>
                        </ui:button>
                    </ui:form>
                </td>
            </ui:tr>
        </g:each>
    </ui:table>

    <div class="row-fluid">
        <div class="span12">
            <h4 class="light-orange">
                考卷情况&nbsp;&nbsp;&nbsp;&nbsp;
                <small>
                    <a href="#addPaper-form" id="addPaper" name="addPaper"  data-toggle="modal" >添加考卷</a>
                </small>
            </h4>
        </div>
    </div>
    <ui:table class="table-striped table-bordered table-hover">
        <ui:tr>
            <th><g:message code="paper.name.label" default="Name"/></th>
            <th><g:message code="paper.extName.label" default="Ext Name"/></th>
            <th><g:message code="paper.inUse.label" default="In Use"/></th>
            <th>Options</th>
        </ui:tr>
        <g:each in="${examInstance.papers}" var="paper">
            <ui:tr>
                <td><f:display bean="${paper}" property="name"/></td>
                <td><f:display bean="${paper}" property="extName"/></td>
                <td><f:display bean="${paper}" property="inUse"/></td>
                <td>
                    <ui:button kind="anchor" mode="info" class="btn-mini" action="edit" controller="paper" id="${paper.id}">
                        <i class="icon-info-sign"></i>
                        <g:message code="default.button.edit.label" default="Edit" />
                    </ui:button>
                    <ui:form action="delete" class="inline" controller="paper">
                        <g:hiddenField name="id" value="${paper.id}"/>
                        <ui:button mode="danger" class="btn-mini" type="submit" name="_action_delete" id="delete-button">
                            <i class="icon-trash icon-white"></i>
                            <g:message code="default.button.delete.label" default="Delete"/>
                        </ui:button>
                    </ui:form>
                </td>
            </ui:tr>
        </g:each>
    </ui:table>

    <div class="row-fluid">
        <div class="span12">
            <h4 class="light-red">
                考生情况&nbsp;&nbsp;&nbsp;&nbsp;
                <small>
                    <g:link action="list" controller="student" params="${[exam: examInstance.id]}" >查看名单</g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                </small>
                <small>
                    <g:link action="studentSeatMap" controller="exam" id="${examInstance.id}" >查看座位分布</g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                </small>
                <small>
                    <g:link action="cleanStudent" controller="exam" id="${examInstance.id}" >清空考生数据</g:link>&nbsp;&nbsp;&nbsp;&nbsp;
                </small>
                <small>
                    <g:link action="exportMoodleUserList" controller="exam" params="${[id: examInstance?.id, format: 'csv', extension: 'csv']}" >下载Moodle数据</g:link>
                    %{--<export:formats formats="['csv']" action="exportMoodleUserList" controller="exam" params="${[id: examInstance?.id]}" />--}%
                </small>
                </h4>
            <div class="row-fluid">
                <div class="span6">
                    <p>
                        <i class="icon-angle-right"></i>
                        考生总人数：${studentTotalCount}&nbsp;&nbsp;&nbsp;&nbsp;
                        <g:link action="randSeat" controller="exam" id="${examInstance.id}">
                            <g:if test="${examInstance.isSorted}">
                                重排座位
                            </g:if>
                            <g:else>
                                生成座位
                            </g:else>
                        </g:link>
                    </p>
                    <p>
                        <i class="icon-angle-right"></i>
                        已排序人数：${studentSortedCount}
                    </p>
                    <p>
                        <i class="icon-angle-right"></i>
                        未排序人数：${studentUnsortedCount}
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="addPaper-form" class="modal hide in" tabindex="-1" aria-hidden="false" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="blue bigger">Please fill the following form fields</h4>
        </div>

        <div class="modal-body overflow-visible">
            <div class="row-fluid">
                <input type="hidden" id="exam_id_for_add_paper" name="exam_id_for_add_paper" value="${examInstance.id}">
                <div class="span12 align-center">
                    <b>name:&nbsp;&nbsp;&nbsp;</b><input type="text" id="new_paper_name" name="new_paper_name" value="">
                </div>
                <div class="span12 align-center">
                    <b>Ext Name:&nbsp;&nbsp;&nbsp;</b><input type="text" id="new_paper_ext_name" name="new_paper_ext_name" value="">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-small" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>

            <button class="btn btn-small btn-primary" id="paperSubmit">
                <i class="icon-ok"></i>
                Save
            </button>
        </div>
    </div>

    <div id="addRoom-form" class="modal hide in" tabindex="-1" aria-hidden="false" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="blue bigger">Please fill the following form fields</h4>
        </div>

        <div class="modal-body overflow-visible">
            <div class="row-fluid">
                <input type="hidden" id="exam_id_for_add_room" name="exam_id_for_add_room" value="${examInstance.id}">
                <div class="span12 align-center">
                    <b>Room:&nbsp;&nbsp;&nbsp;</b><randseat:selectRoom id="room_list_for_add_room" name="room_list_for_add_room" />
                </div>
                <div class="span12 align-center">
                    <b>Seat Number:&nbsp;&nbsp;&nbsp;</b><input type="text" id="new_seat_number" name="new_seat_number" value="">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-small" data-dismiss="modal">
                <i class="icon-remove"></i>
                Cancel
            </button>

            <button class="btn btn-small btn-primary" id="roomSubmit">
                <i class="icon-ok"></i>
                Save
            </button>
        </div>
    </div>
</theme:zone>
<r:script type="text/javascript">
$(document).ready(function () {
    $("#paperSubmit").click(function () {
        var examId = $('#exam_id_for_add_paper').val();
        var paperName = $('#new_paper_name').val();
        var paperExtName = $('#new_paper_ext_name').val();
        if (examId.isNull || examId.undefined || !examId
            || paperName.isNull || paperName.undefined || !paperName
            || paperExtName.isNull || paperExtName.undefined || !paperExtName){
            $.gritter.add({
                title: "参数不能为空！",
                class_name: 'gritter-error'
            });
            return false;
        }
        $.ajax({
            type: 'POST',
            url: '<g:createLink controller="paper" action="addPaper" />',
            data: {exam:examId,name:paperName,extName:paperExtName},
            success: function(data) {
                if (data.code==0){
                    $.gritter.add({
                        title: data.res.message,
                        class_name: 'gritter-error'
                    });
                    return false;
                }
                window.location.reload();
            },
            error: function() {
                $.gritter.add({
                    title: "添加失败",
                    class_name: 'gritter-error'
                });
            }
        });
    });
    $("#roomSubmit").click(function () {
        var examId = $('#exam_id_for_add_room').val();
        var roomId = $('#room_list_for_add_room').val();
        var seatNumber = $('#new_seat_number').val();
        if (examId.isNull || examId.undefined || !examId
            || roomId.isNull || roomId.undefined || !roomId
            || seatNumber.isNull || seatNumber.undefined || !seatNumber || isNaN(seatNumber)){
            $.gritter.add({
                title: "参数不能为空,并且座位数必须为数字",
                class_name: 'gritter-error'
            });
            return false;
        }
        $.ajax({
            type: 'POST',
            url: '<g:createLink controller="examRoom" action="addItem" />',
            data: {exam:examId,room:roomId,number:seatNumber},
            success: function(data) {
                if (data.code==0){
                    $.gritter.add({
                        title: data.res.message,
                        class_name: 'gritter-error'
                    });
                    return false;
                }
                window.location.reload();
            },
            error: function() {
                $.gritter.add({
                    title: "添加失败",
                    class_name: 'gritter-error'
                });
            }
        });
    });
})
</r:script>
</body>
</html>
