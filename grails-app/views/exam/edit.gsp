<%@ page import="randseat.Exam" %>
<%@page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'exam.label', default: 'Exam')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <ui:form action="update" id="${examInstance?.id}" >
    <g:hiddenField name="version" value="${examInstance?.version}"/>
    <fieldset>
        %{--<h4 class="light-blue">基本信息</h4>--}%
        <f:field property="name" bean="examInstance" required="required"/>
        <f:field property="startTime" bean="examInstance" required="required"/>
        <f:field property="endTime" bean="examInstance" required="required"/>
%{--
        <hr />
        <h4 class="light-green">教室使用情况</h4>
        <ui:table class="table-striped table-bordered table-hover" name="rooms" id="rooms">
            <ui:tr>
                <th class="span3"><g:message code="room.label" default="Room" /></th>
                <th class="span3"><g:message code="examRoom.number.label" default="Seat Count" /></th>
                <th class="span3"><g:message code="default.operation.label" default="Operation"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="${examRoomInstanceList}" var="examRoom">
                <ui:tr>
                    <td><g:select name="room" from="${randseat.Room.list()}" optionKey="id" noSelection="['':'选择教室']" value="${examRoom?.room.id}"/> </td>
                    <td><input type="text" name="amount" required="required" value="${examRoom.number}"/></td>
                    <td>
                        <a href="#" class="btn btn-danger btn-mini" id="delete-button" onclick="deleteRow(this)">
                            <i class="icon-trash"></i>
                            <g:message code="default.button.delete.label" default="Delete"/>
                        </a>
                    </td>
                </ui:tr>
            </g:each>
            <ui:tr>
                <td></td>
                <td></td>
                <td>
                    <a href="#" class="btn btn-primary btn-mini" onclick="addRow()">
                        <i class="icon-plus"></i>
                        <g:message code="default.button.add.label" default="Add"/>
                    </a>
                </td>
            </ui:tr>
            </tbody>
        </ui:table>--}%
    </fieldset>
    <ui:actions>
        <ui:button type="submit" mode="primary">
            <i class="icon-ok icon-white"></i>
            <g:message code="default.button.update.label" default="Update"/>
        </ui:button>
        <ui:button type="submit" mode="danger" name="_action_delete" formnovalidate="true" id="delete-button">
            <i class="icon-trash icon-white"></i>
            <g:message code="default.button.delete.label" default="Delete"/>
        </ui:button>
    </ui:actions>
</ui:form>
</theme:zone>
%{--
<r:script type="text/javascript">
    // 初始化行 设置序列号
    function initRows(tab){
        var tabRows = tab.rows.length;
        for(var i = 0; i < tabRows.length; i++){
            tab.rows[i].cells[0].firstChild.value=i;
        }
    }

    // 删除行
    function deleteRow(obj){
        var tab = document.getElementById('rooms');

        // 获取tr对象
        var tr = obj.parentNode.parentNode;
        if(tab.rows.length>2){
            tr.parentNode.removeChild(tr);
        }
        // 重新生成行号
        initRows(document.getElementById('rooms'));
    }

    function addRow(room,amount){
        var tab = document.getElementById('rooms');

        // 添加一行
        var tr  = tab.insertRow(tab.rows.length-1);
        var td1 = tr.insertCell();
        var td2 = tr.insertCell();
        var td3 = tr.insertCell();
        td1.innerHTML = "<select name='room' id='room'> <option value=''>选择教室</option> ${roomOptionStr}</select>";
        td2.innerHTML = "<input type='text' name='amount' value='' required='required' />";
        td3.innerHTML = "<a href='#' class='btn btn-danger btn-mini' onclick='deleteRow(this)'><i class='icon-trash'></i> ${g.message(code: 'default.button.delete.label')}</a>";
        // 初始化行
        initRows(tab);
    }
</r:script>--}%
</body>
</html>
