<%@ page import="randseat.Exam" %>



<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="exam.name.label" default="Name"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" required="" value="${examInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'startTime', 'error')} required">
    <label for="startTime">
        <g:message code="exam.startTime.label" default="Start Time"/>
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="startTime" precision="day"  value="${examInstance?.startTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'endTime', 'error')} required">
    <label for="endTime">
        <g:message code="exam.endTime.label" default="End Time"/>
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="endTime" precision="day"  value="${examInstance?.endTime}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'rooms', 'error')} required">
    <label for="rooms">
        <g:message code="exam.rooms.label" default="Rooms"/>
        <span class="required-indicator">*</span>
    </label>
    
<ul class="one-to-many">
<g:each in="${examInstance?.rooms?}" var="r">
    <li><g:link controller="examRoom" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="examRoom" action="create" params="['exam.id': examInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'examRoom.label', default: 'ExamRoom')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: examInstance, field: 'papers', 'error')} ">
    <label for="papers">
        <g:message code="exam.papers.label" default="Papers"/>
        
    </label>
    
<ul class="one-to-many">
<g:each in="${examInstance?.papers?}" var="p">
    <li><g:link controller="paper" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="paper" action="create" params="['exam.id': examInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'paper.label', default: 'Paper')])}</g:link>
</li>
</ul>


</div>

