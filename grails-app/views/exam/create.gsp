<%@ page import="randseat.Exam" %>
<%@page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'exam.label', default: 'Exam')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link class="btn">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <ui:form action="save" >
    <fieldset>
        %{--<h4 class="light-blue">基本信息</h4>--}%
        <f:field property="name" bean="examInstance" required="required"/>
        <f:field property="startTime" bean="examInstance" required="required"/>
        <f:field property="endTime" bean="examInstance" required="required"/>

        %{--<hr />--}%
        %{--<h4 class="light-green">教室使用情况</h4>--}%
        %{--<ui:table class="table-striped table-bordered table-hover" name="rooms" id="rooms">--}%
            %{--<ui:tr>--}%
                %{--<th class="span3"><g:message code="room.label" default="Room" /></th>--}%
                %{--<th class="span3"><g:message code="examRoom.number.label" default="Seat Count" /></th>--}%
                %{--<th class="span3"><g:message code="default.operation.label" default="Operation"/></th>--}%
            %{--</ui:tr>--}%
            %{--</thead>--}%
            %{--<tbody>--}%
            %{--<ui:tr>--}%
                %{--<td></td>--}%
                %{--<td></td>--}%
                %{--<td>--}%
                    %{--<a href="#" class="btn btn-primary btn-mini" onclick="addRow()">--}%
                        %{--<i class="icon-plus"></i>--}%
                        %{--<g:message code="default.button.add.label" default="Add"/>--}%
                    %{--</a>--}%
                %{--</td>--}%
            %{--</ui:tr>--}%
            %{--</tbody>--}%
        %{--</ui:table>--}%
    </fieldset>
    <ui:actions>
        <ui:button type="submit" mode="primary">
            <i class="icon-ok icon-white"></i>
            <g:message code="default.button.create.label" default="Create"/>
        </ui:button>
    </ui:actions>
</ui:form>
</theme:zone>
%{--

<r:script type="text/javascript">
    // 初始化行 设置序列号
    function initRows(tab){
        var tabRows = tab.rows.length;
        for(var i = 0; i < tabRows.length; i++){
            tab.rows[i].cells[0].firstChild.value=i;
        }
    }

    // 删除行
    function deleteRow(obj){
        var tab = document.getElementById('rooms');

        // 获取tr对象
        var tr = obj.parentNode.parentNode;
        if(tab.rows.length>2){
            tr.parentNode.removeChild(tr);
        }
        // 重新生成行号
        initRows(document.getElementById('rooms'));
    }

    function addRow(room,amount){
        var tab = document.getElementById('rooms');

        // 添加一行
        var tr  = tab.insertRow(tab.rows.length-1);
        var td1 = tr.insertCell();
        var td2 = tr.insertCell();
        var td3 = tr.insertCell();
        td1.innerHTML = "<select name='room' id='room'> <option value=''>选择教室</option> ${roomOptionStr}</select>";
        td2.innerHTML = "<input type='text' name='amount' value='' required='required' />";
        td3.innerHTML = "<a href='#' class='btn btn-danger btn-mini' onclick='deleteRow(this)'><i class='icon-trash'></i> ${g.message(code: 'default.button.delete.label')}</a>";
        // 初始化行
        initRows(tab);
    }
</r:script>
--}%

</body>
</html>
