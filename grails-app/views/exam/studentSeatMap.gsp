
<%@ page import="randseat.Exam" %>
<%@ page defaultCodec="none" %>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Exam')}"/>
    <title>考生分布图</title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="show" controller="exam" id="${examRoomInstanceList[0]?.exam?.id}">
            考试详情
        </ui:button>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <div>
        <div class="tabbable">
            <ul class="nav nav-tabs" id="myTab">
                <g:if test="${examRoomInstanceList}" >
                    <g:each in="${examRoomInstanceList}" var="examRoom" status="i">
                        <li class="${i==0?'active':''}">
                            <a data-toggle="tab" href="#home_${i}">
                                <i class="green icon-home bigger-110"></i>
                                ${examRoom.room.name}
                            </a>
                        </li>
                    </g:each>
                </g:if>
            </ul>

            <div class="tab-content">
                <g:if test="${seatMaps}" >
                    <g:each in="${seatMaps}" var="seatMap" status="i">
                        <div id="home_${i}" class="tab-pane in ${i==0?'active':''}">
                            ${seatMap}
                        </div>
                    </g:each>
                </g:if>
            </div>
        </div>
    </div>
</theme:zone>
</body>
</html>
