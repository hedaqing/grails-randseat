package randseat

class ExamRoom {

    Integer number

    static belongsTo = [exam: Exam, room: Room]
    static constraints = {
        number nullable: false
        exam nullable: false
        room nullable: false
    }

    static mapping = {
        number comment: '考场人数'
        exam comment: '考试'
        room comment: '考场'
    }

    @Override
    public String toString() {
        return exam.name+ ": " + room.name+": "+number+"人"
    }
}
