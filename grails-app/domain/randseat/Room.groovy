package randseat

class Room {

    String name
    String place
    String code
    Integer seatCount
    Integer width
    Integer height

    static hasMany = [ranges: Ranges,exams: ExamRoom,students:Student]

    static constraints = {
        name nullable: false
        place nullable: false
        code nullable: false
        seatCount nullable: false
        width nullable: false
        height nullable: false
        ranges nullable: true
        exams nullable: true
        students nullable: true
    }
    static mapping = {
        name comment: '考场名称'
        place comment: '考场地点'
        code comment: '考场编码'
        seatCount comment: '座位数'
        width comment: '考场宽座位数'
        height comment: '考场长座位数'
        ranges comment: '考场区域数'
        exams comment: '考场考试集合'
        students comment: '在该考场的学生'
    }

    @Override
    public String toString() {
        return name+"("+seatCount+")"
    }
}
