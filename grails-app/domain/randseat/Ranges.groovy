package randseat

class Ranges {

    String name
    Integer seq
    Integer row
    Integer col
    Integer width
    Integer height

    static belongsTo = [room: Room]
    static hasMany = [seats: Seat]
    static constraints = {
        name nullable: false
        room nullable: false
        seq nullable: false
        row nullable: false
        col nullable: false
        width nullable: false
        height nullable: false
    }

    static mapping = {
        name comment: '区域名称'
        room comment: '所属教室'
        seq comment: '区域编号'
        row comment: '横向起点坐标'
        col comment: '纵向起点坐标'
        width comment: '区域纵向座位数'
        height comment: '区域横向座位数'
    }

    @Override
    public String toString() {
        return name
    }
}
