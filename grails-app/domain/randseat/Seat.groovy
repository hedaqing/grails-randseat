package randseat

class Seat {

    Integer code
    Integer row
    Integer col
    boolean usable=true

    static belongsTo = [range: Ranges]

    static hasMany = [students:Student]

    static constraints = {
        code nullable: false
        row nullable: false
        col nullable: false
        students nullable: true
        usable nullable: false
    }

    static mapping = {
        code comment: '座位编号'
        row comment: '座位横向坐标'
        col comment: '座位纵向坐标'
        usable comment: '是否可用'
        students comment: '该座位的学生'
    }

    @Override
    public String toString() {
        return "Seat{" + range + code + '}'
    }

    Student getStudent(Exam exam){
        return Student.findByExamAndSeat(exam,this)
    }
}
