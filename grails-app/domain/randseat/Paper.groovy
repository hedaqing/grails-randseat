package randseat

class Paper {

    String name
    String extName
    boolean inUse=true

    static belongsTo = [exam: Exam]

    static hasMany = [students:Student]

    static constraints = {
        exam nullable: false
        name nullable: false
        extName nullable: false
        inUse nullable: false
        students nullable: true
    }

    static mapping = {
        exam comment: '考试'
        name comment: '试卷名称'
        extName comment: '外部名称'
        inUse comment: '是否使用'
        students comment: '该考卷的学生'
    }

    @Override
    public String toString() {
        return name+"("+extName+")"
    }
}
