package randseat

class Student {

    String username
    String idnumber
    String firstname
    String lastname
    String password
    String email
    String department
    boolean isSorted=false

    static belongsTo = [exam: Exam,room: Room,seat: Seat,paper:Paper]

    static constraints = {
        username nullable: false
        idnumber nullable: false
        firstname nullable: false
        lastname nullable: false
        password nullable: true
        email nullable: true
        department nullable: true
        exam nullable: true
        room nullable: true
        seat nullable: true
        paper nullable: true
        isSorted nullable: false
    }

    static mapping = {
        username comment: '用户名'
        idnumber comment: '学号'
        firstname comment: '名字'
        lastname comment: '姓氏'
        password comment: '密码'
        email comment: '邮箱'
        department comment: '班级'
        exam comment: '考试'
        room comment: '考场'
        seat comment: '座位'
        paper comment: '考卷'
        isSorted comment: '是否已经排座位'
    }
}
