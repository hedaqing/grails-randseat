package randseat

class Exam {

    String name
    Date startTime
    Date endTime
    boolean isSorted=false

    static hasMany = [papers: Paper,rooms: ExamRoom,students:Student]

    static constraints = {
        name nullable: false
        startTime nullable: false
        endTime nullable: false
        rooms nullable: true
        papers nullable: true
        isSorted nullable: false
        students nullable: true
    }

    static mapping = {
        name comment: '考试名称'
        startTime comment: '考试开始时间'
        endTime comment: '考试结束时间'
        rooms comment: '考场'
        papers comment: '考卷'
        isSorted comment: '是否完成排座位'
        students comment: '参加该考试的学生'
    }

    @Override
    public String toString() {
        return name
    }
}
