<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <g:link action="create" class="btn btn-primary">
            <g:message code="default.button.create.label" default="Create"/>
        </g:link>
    </theme:zone>
    <ui:displayMessage/>
    <div id="flip-scroll">
        <ui:table class="table-bordered table-hover">
            <thead>
            <ui:tr>
                <% excludedProps = Event.allEvents.toList() << 'version' << 'dateCreated' << 'lastUpdated'
                allowedNames = domainClass.persistentProperties*.name << 'id'
                props = domainClass.properties.findAll {
                    allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type)
                }
                Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                props.eachWithIndex { p, i ->
                    if (i < 8) {
                        if (p.isAssociation()) { %>
                <th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></th>
                <% } else { %>
                <g:sortableColumn property="${p.name}"
                                  title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${
                                          p.naturalName}')}" params="\${params}"/>
                <% }
                }
                } %>
                <th><g:message code="default.actions.label" default="Actions"/></th>
            </ui:tr>
            </thead>
            <tbody>
            <g:each in="\${${propertyName}List}" var="${propertyName}">
                <ui:tr>
                    <% props.eachWithIndex { p, i ->
                        if (i < 8) {
                            if (p.type == Boolean || p.type == boolean) { %>
                    <td><g:formatBoolean boolean="\${${propertyName}.${p.name}}"/></td>
                    <%
                        } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
                    <td><g:formatDate date="\${${propertyName}.${p.name}}"/></td>
                    <% } else { %>
                    <td><f:display bean="\${${propertyName}}" property="${p.name}"/></td>
                    <% }
                    }
                    } %>
                    <td class="link">
                        <ui:button kind="anchor" mode="info" class="btn-mini" action="show" id="\${${propertyName}.id}">
                            <i class="icon-info-sign"></i>
                            <g:message code="default.button.show.label" default="Show"/>
                        </ui:button>
                    </td>
                </ui:tr>
            </g:each>
            </tbody>
        </ui:table>
    </div>

    <div class="pagination">
        <ui:paginate total="\${${propertyName}Total}" params="\${params}"/>
    </div>
</theme:zone>
</body>
</html>
