<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
<head>
    <theme:layout name="dataentry"/>
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="application"/>
</head>

<body>
<theme:zone name="body">
    <theme:zone name="header-actions">
        <ui:button kind="anchor" mode="primary" action="edit" id="\${${propertyName}?.id}">
            <i class="icon-pencil"></i>
            <g:message code="default.button.edit.label" default="Edit"/>
        </ui:button>
        <ui:form action="delete" class="inline">
            <g:hiddenField name="id" value="\${${propertyName}?.id}"/>
            <ui:button mode="danger" type="submit" name="_action_delete" id="delete-button">
                <i class="icon-trash icon-white"></i>
                <g:message code="default.button.delete.label" default="Delete"/>
            </ui:button>
        </ui:form>
        <ui:button kind="anchor">
            <i class="icon-arrow-left"></i>
            <g:message code="default.button.return.label" default="Return"/>
        </ui:button>
    </theme:zone>
    <ui:displayMessage/>
    <dl>
        <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
        allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
        props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
        Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
        props.each { p -> %>
        <g:if test="\${${propertyName}?.${p.name}}">
            <dt><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}"/></dt>
            <% if (p.isEnum()) { %>
            <dd><g:fieldValue bean="\${${propertyName}}" field="${p.name}"/></dd>
            <% } else if (p.oneToMany || p.manyToMany) { %>
            <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
                <dd><g:link controller="${p.referencedDomainClass?.propertyName}" action="show"
                            id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></dd>
            </g:each>
            <% } else if (p.manyToOne || p.oneToOne) { %>
            <dd><g:link controller="${p.referencedDomainClass?.propertyName}" action="show"
                        id="\${${propertyName}?.${p.name}?.id}"><f:display bean="\${${propertyName}}"
                                                                           property="${p.name}"/></g:link></dd>
            <% } else if (p.type == Boolean || p.type == boolean) { %>
            <dd><g:formatBoolean boolean="\${${propertyName}?.${p.name}}"/></dd>
            <%
                } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
            <dd><g:formatDate date="\${${propertyName}?.${p.name}}"/></dd>
            <% } else if (!p.type.isArray()) { %>
            <dd><f:display bean="\${${propertyName}}" property="${p.name}"/></dd>
            <% } %>
        </g:if>
        <% } %>
    </dl>

</theme:zone>
</body>
</html>
